﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class UserRequest : HisCV
    {
        public LinkedList<LinkedList<Models.User>> users { get; set; }
        public Models.User loggedIn { get; set; }
    }
}