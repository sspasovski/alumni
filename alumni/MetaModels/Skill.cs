﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class Skill
    {
        public String description { get; set; }
        public String title { get; set; }

        public Skill(String d, String t)
        { description = d; title = t; }
    }
}