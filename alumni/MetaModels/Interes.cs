﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class Interes
    {
        public String description { get; set; }
        public String title { get; set; }

        public Interes(String d, String t)
        {description = d; title = t;}
    }
}