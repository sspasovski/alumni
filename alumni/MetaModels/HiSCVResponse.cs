﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class HiSCVResponse
    {
        public string name { get; set; }
        public string surname { get; set; }
        public char gender { get; set; }
        public string picture_url { get; set; }
        public string placeOfBirth { get; set; }
        public string dateOfBirth { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public int martial { get; set; }//id na martial status

        public int nationalityID { get; set; }//id na nationality 

        public string nationality { get; set; }//nationality 

        public string characteristics { get; set; }// kar1|kar2|kart3|kar4...

        public string courses { get; set; }// kurs1_ime#kurs1_opis#kurs1_od#|kurs2_ime#kurs2_opis#kurs2_od#kurs2_do|kurs3...

        public string achievements { get; set; }// ach1_ime#ach1_opis#ach1_koga|...

        public string interests { get; set; } //inter1_title#inter1_opis|inter2_title#inter2_opis|...

        public string skills { get; set; } //sk1_id#sk1_title#sk1_opis|..
        //sk1_id e id na skillType-ot odi vo SkillType
        //sk1_title e opisot na istiot vo SkillType
        //sk1_opis e opisot vo Skils vo Skill

        //0#sk1_title#sk1_opis|1#nenitno(inace e C)#rokam u c

        public string educationInstitutions { get; set; } // eduTipId#naziv_id#naziv_title#od#do{moze i prazno}#opis|...
        //eduTipId se cita od baza i e samo edno id
        //naziv_id ako e 0 se kreira nov, se mechuva so ajdi-to so treba da go ima
        //naziv_title vazno samo ako naziv_id==0 odi vo EducationInstitution

        //od 
        //do moze i prazno
        //i opis odat vo HistoryCVEducation Institution

        public string work { get; set; } // inst_id#inst_title#od#do{moze i prazno|...
        //inst_id e institucija na id
        //inst_title e vazno samo koga inst_id==0 vo WorkInstitution
        //od
        //do vo CVWorkInstitution

        public string project { get; set; } // project1_title#project1_descr#proj1_user1#project1_user2...|...

        public string categoriesInCV { get; set; } // catID1|catID2|catID3|...


        //example
        /*
         *      achievements: "#2ro mesto republicki matematika#30ka na LoL#Beta key sa kotor mmorgp-to|#05.05.2007#09.01.2011#09.01.2011|##poso vredi :D#samo ogranicen broj lugje go imaat ova, i jas sum 1 od niv"
                address: null
                categoriesInCV: null
                characteristics: "#Rekurzii mu idat#Gets stuff done#Team player"
                courses: "#ICK#KOS#Bazi 2|#bless ya#poso me potsekja na kosinus#Ko bazi 1 ali baegi poteshko|#09.01.2011#09.01.2011#09.01.2011|#12.12.2011#12.12.2011#12.12.2011"
                dateOfBirth: "21.03.1989"
                educationInstitutions: "#2#1#1|#3^Orce#1^PMF#2^FINKI|#09.01.2004#09.01.2008#09.09.2011|#06.09.2008#20.09.2011#10.09.2012|#Odlicen ucenik#Top student, jak prosek, se iznagejmav#Top student, jak prosek, se pomalce gejmav poso bev i demonstrator vo isto vreme"
                email: "shpicajzla@bossmail.com"
                gender: 109 'm'
                interests: "#Gejmanje#Programiranje|#Gejmanje, igranje kumjuterski igrici kako LoL (League of Legends), WoW (World of Warcraft), Skyrim, Mustafa (Cadilacs & Dinosaurs)...#Programiranje vo Java"
                martial: 2
                name: "Stefan"
                nationality: "1^Makedonija"
                nationalityID: 0
                phone: "077 743 293"
                picture_url: "https://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-snc1/9334_1224506169080_1121781021_1783267_1922160_n.jpg"
                placeOfBirth: "Skopje"
                project: "# d|# d|#new1^1#new1^2"
                skills: "#3^Android Aplikacii#1^C#2^Java|#Kreiranje android aplikacii#C kodiranje#Java kodiranje"
                surname: "Spasovski"
                work: "#2^Netcetera|#07.01.2011|#09.01.2011"
         */
    }
}