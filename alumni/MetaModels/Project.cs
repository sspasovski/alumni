﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class Project
    {
        public List<Models.User> users { get; set; }
        public String description { get; set; }
        public String title { get; set; }

        public Project(List<Models.User> lu, String d, String t)
        {users = lu; description = d; title = t;}
    }
}