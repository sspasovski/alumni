﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class EducationInstitution
    {
        public String title { get; set; }
        public String text { get; set; }

        public EducationInstitution(String ti, String te)
        {title = ti; text = te;}
    }
}