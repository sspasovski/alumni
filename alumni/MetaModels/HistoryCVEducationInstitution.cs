﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class HistoryCVEducationInstitution
    {
        public EducationInstitution educationInstitution { get; set; }
        public String description { get; set; }
        public DateTime from { get; set; }
        public DateTime? to { get; set; }

        public HistoryCVEducationInstitution(EducationInstitution e, String d, DateTime p, DateTime? k)
        {
            educationInstitution = e;
            description = d;
            from = p; to = k;
        }
    }
}