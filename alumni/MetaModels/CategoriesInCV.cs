﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class CategoriesInCV
    {
        public String name { get; set; }
        public bool subscribed { get; set; }

        public CategoriesInCV(String n, bool s)
        { name = n; subscribed = s; }
    }
}