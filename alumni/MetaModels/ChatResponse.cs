﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class ChatResponse
    {
        public String content { get; set; }
        public int withUser { get; set; }
        public int action { get; set; }
    }
}