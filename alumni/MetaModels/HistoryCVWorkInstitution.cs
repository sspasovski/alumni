﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class HistoryCVWorkInstitution
    {
        public String title { get; set; }
        public DateTime from { get; set; }
        public DateTime? to { get; set; }

        public HistoryCVWorkInstitution(String t, DateTime p, DateTime? k)
        { title = t; from = p; to = k; }
    }
}