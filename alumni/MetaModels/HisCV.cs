﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using alumni;

namespace alumni.MetaModels
{
    public class HisCV
    {
        public Models.User user { get; set; }
        public String placeOfBirth { get; set; }
        public DateTime dateOfBirth { get; set; }
        public String address { get; set; }
        public String phone { get; set; }
        public String email { get; set; }
        public Models.Martial_status_option martialStatus { get; set; }
        public Models.Nationality_option nationality { get; set; }

        public List<Models.Personal_Characteristic> characteristics { get; set; }
        public List<Models.Course> courses { get; set; }
        public List<Models.Achievement> achievements { get; set; }

        public List<MetaModels.Interes> interests { get; set; }
        public List<MetaModels.Skill> skills { get; set; }
        public List<MetaModels.HistoryCVEducationInstitution> educationInstitutions { get; set; }
        public List<MetaModels.HistoryCVWorkInstitution> workInstitutions { get; set; }
        public List<MetaModels.Project> projects { get; set; }
        public List<MetaModels.CategoriesInCV> categoriesInCV { get; set; }

        public List<Models.Nationality_option> nacionalnostDDL { get; set; }
        public List<Models.Martial_status_option> martialDDL { get; set; }
        public List<Models.Education_Type> EduTypeDDL { get; set; }
        public List<Models.Education_institution> EduInstDDL { get; set; }
        public List<Models.Work_institution> WorkInstDDL { get; set; }
        public List<Models.Skill_Type> SkillsDDL { get; set; }
        public List<Models.Interes_Type> InteresDDL { get; set; }
    }
}