﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class WallResponse
    {
        public int action { get; set; } //0 notif
                                 //1 request
                                 //2 comment on req
                                 //3 comment on comment
                                 //4 click on group
                                //5 brishenje na request
                                //6 brishenje na komentar
                                //7 notif
        public string from_to { get; set; }
                                 //from % to
        public int user_id { get; set; }//for logged in user

        public int comment_id { get; set; }//if comment is in reply to comment with id

        public int choosen_category { get; set; }

        public string notif_content { get; set; }
        public string response_text { get; set; }


        public int response_to_req_id { get; set; }

        public int delete_id { get; set; }

    }
}