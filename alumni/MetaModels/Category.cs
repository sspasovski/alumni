﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using alumni.Models;

namespace alumni.MetaModels
{
    public class Category
    {
        public Category()
        {
            notifications = new LinkedList<Notification>();
        }
        public LinkedList<Notification> notifications { get; set; }
        public int unread { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public int id { get; set; }
    }
}