﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using alumni.Models;

namespace alumni.MetaModels
{
    public class Reply
    {
        public Reply()
        {
            replays = new LinkedList<Reply>();
        }
        public int id { get; set; }
        public Reply father { get; set; }
        public LinkedList<Reply> replays { get; set; }
        public String content { get; set; }
        public User user { get; set; }
        public DateTime when { get; set; }
    }
}