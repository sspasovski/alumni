﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class Message
    {
        public Models.User from_user { get; set; }
        public Models.User to_user { get; set; }
        public DateTime when { get; set; }
        public string content { get; set; }

        public Message(Models.User user, Models.User user2, DateTime dateTime, string p)
        {
            // TODO: Complete member initialization
            this.from_user = user;
            this.to_user = user2;
            this.when = dateTime;
            this.content = p;
        }


    }
}