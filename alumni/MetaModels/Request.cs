﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using alumni.Models;

namespace alumni.MetaModels
{
    public class Request : Notification
    {
        public Request()
        {
            replays = new LinkedList<Reply>();
        }
        public int id { get; set; }
        public LinkedList<Reply> replays { get; set; }
        public DateTime available_from { get; set; }
        public DateTime available_to { get; set; }

    }
}