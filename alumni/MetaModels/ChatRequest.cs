﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using alumni;

namespace alumni.MetaModels
{
    public class ChatRequest
    {
        public ChatRequest()
        {
            users =new LinkedList<LinkedList<MetaModels.UserWithUnread>>();
        }
        //users are sorted in database layer with order by
        public LinkedList<LinkedList<MetaModels.UserWithUnread>> users { get; set; }
        public List<MetaModels.Message> messages { get; set; }
        public Models.User withUser { get; set; }
        public Models.User loggedInUser { get; set; }
        public String time { get; set; }
    }
}