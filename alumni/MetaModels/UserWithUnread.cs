﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.MetaModels
{
    public class UserWithUnread
    {
        public int unread { get; set; }
        public Models.User user { get; set; }
    }
}