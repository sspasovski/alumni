﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using alumni.Models;

namespace alumni.MetaModels
{
    public class WallRequest
    {
        public WallRequest()
        {
            categories = new LinkedList<Category>();
        }
        public LinkedList<Category> categories { get; set; }
        public User logged_in { get; set; }
        public int choosen_category { get; set; }
    }
}