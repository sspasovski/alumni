﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using alumni.Models;

namespace alumni.MetaModels
{
    public class Notification
    {
        public DateTime when { get; set; }
        public string content { get; set; }
        public User user { get; set; }
        public int id { get; set; }
    }
}