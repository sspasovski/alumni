﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Interes_Type
    {
        public int ID { get; set; }
        public string title { get; set; }

        public Interes_Type()
        { }

        public Interes_Type(int ID, string title)
        {
            this.ID = ID;
            this.title = title;
        }
    }
}