﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class History_CV_Education_institution
    {
        public int History_CVID { get; set; }
        public int Education_institutionID { get; set; }
        public string description { get; set; }
        public DateTime from_date { get; set; }
        public DateTime? to { get; set; }

        public History_CV_Education_institution()
        { }

        public History_CV_Education_institution(int History_CVID, int Education_InstitutionID, string description, DateTime form_date, DateTime? to)
        {
            this.History_CVID = History_CVID;
            this.Education_institutionID = Education_InstitutionID;
            this.description = description;
            this.from_date = form_date;
            this.to = to;
        }
    }
}