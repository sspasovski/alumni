﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Description
    {
        public int User_ProjectID { get; set; }
        public string description { get; set; }

        public Description()
        { }

        public Description(int User_ProjectID, string description)
        {
            this.User_ProjectID = User_ProjectID;
            this.description = description;
        }
    }
}