﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Education_Type
    {
        public int ID { get; set; }
        public string text { get; set; }

        public Education_Type()
        { }

        public Education_Type(int ID, string text)
        {
            this.ID = ID;
            this.text = text;
        }
    }
}