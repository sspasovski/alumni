﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Education_institution
    {
        public int ID { get; set; }
        public string title { get; set; }
        public int Education_TypeID { get; set; }

        public Education_institution()
        { }

        public Education_institution(int ID, string title, int Education_TypeID)
        {
            this.ID = ID;
            this.title = title;
            this.Education_TypeID = Education_TypeID;
        }
    }
}