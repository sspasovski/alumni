﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Notification
    {
        public int id{ get; set; }
        public int groupTypeId{ get; set; }
        public int userId{ get; set; }
        public string content { get; set; }
        public DateTime when { get; set; }

        public Notification(int id,int groupTypeId,int userId,string content,DateTime when)
        {
            this.id = id;
            this.groupTypeId = groupTypeId;
            this.userId = userId;
            this.content = content;
            this.when = when;
        }
        public Notification()
        {
           
        }
    }

}