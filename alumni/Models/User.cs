﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public char gender { get; set; }
        public int year_attending { get; set; }
        public int? year_graduation { get; set; }
        public string picture_url { get; set; }

        public User(int id, string name, string surname, char gender, int year_attending, int? year_graduation, string picture_url)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.gender = gender;
            this.year_attending = year_attending;
            this.year_graduation = year_graduation;
            this.picture_url = picture_url;
        }
        public User()
        {
          
        }
    }
}