﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Group_Type
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string url { get; set; }

        public Group_Type()
        { }

        public Group_Type(int ID, string name, string url)
        {
            this.ID = ID;
            this.name = name;
            this.url = url;
        }
    }
}