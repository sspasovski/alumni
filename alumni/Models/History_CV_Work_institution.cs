﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class History_CV_Work_institution
    {
        public int History_CVID { get; set; }
        public int Work_institutionID { get; set; }
        public DateTime from_date { get; set; }
        public DateTime? to { get; set; }

        public History_CV_Work_institution()
        { }

        public History_CV_Work_institution(int History_CVID, int Work_IstitutionID, DateTime from_date, DateTime? to)
        {
            this.History_CVID = History_CVID;
            this.Work_institutionID = Work_IstitutionID;
            this.from_date = from_date;
            this.to = to;
        }
    }
}