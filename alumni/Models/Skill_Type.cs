﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Skill_Type
    {
        public int ID { get; set; }
        public string title { get; set; }
        
        public Skill_Type(int ID, string title)
        {
            this.ID = ID;
            this.title = title;
        }
        public Skill_Type()
        {
          
        }
    }
}