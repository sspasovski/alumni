﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Project
    {
        public int ID { get; set; }
        public int History_CVID { get; set; }
        public string title { get; set; }
        public Project(int ID,int History_CVID,string title)
        {
            this.ID = ID;
            this.History_CVID = History_CVID;
            this.title = title; 
        }
        public Project()
        {
         
        }
    }
}