﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class User_Project
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int ProjectID { get; set; }

        public User_Project(int ID,int UserID,int ProjectID)
        {
            this.ID = ID;
            this.UserID = UserID;
            this.ProjectID = ProjectID;
        }
        public User_Project()
        {
            
        }
    }
}