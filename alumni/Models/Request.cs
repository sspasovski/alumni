﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Request
    {
        public int NotificationID { get; set; }
        public DateTime avaliable_from { get; set; }
        public DateTime avaliable_to { get; set; }

        public Request(int NotificationID, DateTime avaliable_from, DateTime avaliable_to)
        {
            this.NotificationID = NotificationID;
            this.avaliable_from = avaliable_from;
            this.avaliable_to = avaliable_to;
        }
        public Request()
        {
           
        }
    }
}