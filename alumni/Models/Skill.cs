﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Skill
    {
        public int History_CVID { get; set; }
        public int Skill_TypeID { get; set; }
        public string description { get; set; }

        public Skill(int History_CVID,int Skill_TypeID,string description)
        {
            this.History_CVID = History_CVID;
            this.Skill_TypeID = Skill_TypeID;
            this.description = description;
        }
        public Skill()
        {
         
        }
    }
}