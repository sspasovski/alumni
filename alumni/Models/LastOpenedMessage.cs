﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class LastOpenedMessage
    {
        public int userId1 { get; set; }
        public int userId2 { get; set; }
        public DateTime when { get; set; }

        public LastOpenedMessage()
        { }

        public LastOpenedMessage(int userId1, int userId2, DateTime when)
        {
            this.userId1 = userId1;
            this.userId2 = userId2;
            this.when = when;
        }
    }
}