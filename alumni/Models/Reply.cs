﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Reply
    {
        public Reply(int? ReqestNotificationID, int? ReplyID, int UserID, string content, DateTime when)
        {
            this.RequestNotiﬁcationID = ReqestNotificationID;
            this.ReplyID = ReplyID;
            this.UserID = UserID;
            this.content = content;
            this.when = when;
        }
        public Reply()
        {

        }
        public int ID { get; set; }
        public int? RequestNotiﬁcationID { get; set; }
        public int? ReplyID { get; set; }
        public int UserID { get; set; }
        public string content { get; set; }
        public DateTime when { get; set; }
    }
}