﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Achievement
    {


        public int ID { get; set; }
        public int History_CVID { get; set; }
        public string what { get; set; }
        public string description { get; set; }
        public DateTime when { get; set; }

        public Achievement()
        { }

        public Achievement(int ID, int History_CVID, string what, string description, DateTime when)
        {
            this.ID = ID;
            this.History_CVID = History_CVID;
            this.what = what;
            this.description = description;
            this.when = when;
        }
    }
}