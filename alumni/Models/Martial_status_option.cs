﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Martial_status_option
    {
        public int ID { get; set; }
        public string text { get; set; }
        public Martial_status_option(int ID,string text)
        {
            this.ID = ID;
            this.text = text;
        }
        public Martial_status_option()
        {
            
        }
    }
}