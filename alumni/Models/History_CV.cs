﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class History_CV
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public DateTime? date_of_birth { get; set; }
        public string place_of_birth { get; set; }
        public int Martial_status_optionID { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public int Nationality_optionaID { get; set; }
        public string email { get; set; }

        public History_CV()
        { }

        public History_CV(int ID, int UserID, DateTime date_of_birth, string place_of_birth, int Martial_status_optionID, string address, string phone, int Nationality_optionID, string email)
        {
            this.ID = ID;
            this.UserID = UserID;
            this.date_of_birth = date_of_birth;
            this.place_of_birth = place_of_birth;
            this.Martial_status_optionID = Martial_status_optionID;
            this.address = address;
            this.phone = phone;
            this.Nationality_optionaID = Nationality_optionID;
            this.email = email;
        }
    }
}