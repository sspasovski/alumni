﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Categories_in_CV
    {
        public int Group_TypeID { get; set; }
        public int History_CVID { get; set; }
        public bool subscribed { get; set; }

        public Categories_in_CV()
        { }

        public Categories_in_CV(int Group_TypeID, int History_CVID, bool subscribed)
        {
            this.Group_TypeID = Group_TypeID;
            this.History_CVID = History_CVID;
            this.subscribed = subscribed;
        }
    }
}