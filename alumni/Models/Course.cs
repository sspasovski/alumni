﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Course
    {
        public int ID { get; set; }
        public int History_CVID { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public DateTime from_date { get; set; }
        public DateTime? to { get; set; }

        public Course()
        { }

        public Course(int ID, int History_CVID, string title, string description, DateTime from_date, DateTime? to)
        {
            this.ID = ID;
            this.History_CVID = History_CVID;
            this.title = title;
            this.description = description;
            this.from_date = from_date;
            this.to = to;
        }
    }
}