﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Message
    {
        public int userID { get; set; }
        public int userID2 { get; set; }
        public DateTime when { get; set; }
        public string content { get; set; }

        public Message(int userID, int userID2, DateTime when, string content)
        {
            // TODO: Complete member initialization
            this.userID = userID;
            this.userID2 = userID2;
            this.when = when;
            this.content = content;
        }

        public Message() { }
    }
}