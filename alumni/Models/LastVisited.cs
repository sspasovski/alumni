﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class LastVisited
    {
        public int userID { get; set; }
        public int groupTypeID { get; set; }
        public DateTime? when { get; set; }
        public LastVisited(int userID,int groupTypeID,DateTime? when)
        {
            this.userID = userID;
            this.groupTypeID = groupTypeID;
            this.when = when;
        }
        public LastVisited()
        {
            
        }
    }
}