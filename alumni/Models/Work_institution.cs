﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Work_institution
    {
        public int ID { get; set; }
        public string title { get; set; }

        public Work_institution (int ID, string title)
        {
            this.ID = ID;
            this.title=title;
        }
        public Work_institution()
        {

        }
    }
}