﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Personal_Characteristic
    {
        public int History_CVID { get; set; }
        public int order { get; set; }
        public string title { get; set; }

        public Personal_Characteristic(int History_CVID,int order,string title)
        {
            this.History_CVID = History_CVID;
            this.order = order;
            this.title = title;
        }
        public Personal_Characteristic()
        {
            
        }
    }
}