﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Interes
    {
        public int History_CVID { get; set; }
        public int Interes_TypeID { get; set; }
        public string description { get; set; }

        public Interes()
        { }

        public Interes(int History_CVID, int Interes_TypeID, string description)
        {
            this.History_CVID = History_CVID;
            this.Interes_TypeID = Interes_TypeID;
            this.description = description;
        }
    }
}