﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace alumni.Models
{
    public class Nationality_option
    {
        public int ID { get; set; }
        public string text { get; set; }
        public Nationality_option(int ID,string text)
        {
            this.ID = ID;
            this.text = text;
        }
        public Nationality_option()
        {

        }
    }
}