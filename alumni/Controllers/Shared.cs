﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using alumni;

namespace alumni.Controllers
{
    public class Shared
    {

        public static repository.FakeRepository repo = new repository.FakeRepository();

        public static List<Models.User> sortedUsers = new List<Models.User>((from tmp in repo.getUsers()
                                                orderby tmp.name
                                                select tmp).ToList());

        public static LinkedList<LinkedList<Models.User>> getUsersByGeneration()
        {
            LinkedList<LinkedList<Models.User>> users = new LinkedList<LinkedList<Models.User>>();

            var tmpUsers =
                (from tmp in repo.getUsers()
                 orderby tmp.year_attending ascending
                 select tmp);

            int tmp_year = tmpUsers.First().year_attending;
            LinkedList<Models.User> tmp_users = new LinkedList<Models.User>();

            //iteracija niz site korisnici
            foreach (Models.User tmp in tmpUsers)
            {
                //bidejki se podredeni, dokolku se smeni godinata pri listanjeto znaci stanuva zbor
                //za student od druga generacija i go pomestuvame vo druga lista

                //ako e ista godinata dodaj go vo privremenata lista so studenti od tmp.year_attending
                if (tmp_year == tmp.year_attending)
                    tmp_users.AddLast(tmp);

                //ako ne togas obnovi ja listata so privremeni user-i i smeni ja godinata na generacijata,
                //dodaj gi prethodnite vo glavnata link lista od link listi
                else
                {
                    users.AddLast(tmp_users);
                    tmp_users = new LinkedList<Models.User>();
                    tmp_year = tmp.year_attending;
                    tmp_users.AddLast(tmp);
                }
            }
            //poslednata link lista od studenti (t.e. studenti od poslednata generacija
            users.AddLast(tmp_users);
            //zamena za garbage collector
            tmp_users = new LinkedList<Models.User>();

            return users;
        }

        public static LinkedList<LinkedList<MetaModels.UserWithUnread>> getUsersWithUnreadByGeneration(int userID)
        {
            LinkedList<LinkedList<MetaModels.UserWithUnread>> users = new LinkedList<LinkedList<MetaModels.UserWithUnread>>();

            var tmpUsers =
                (from tmp in repo.getUsers()
                 orderby tmp.year_attending ascending
                 select tmp);

            int tmp_year = tmpUsers.First().year_attending;
            LinkedList<MetaModels.UserWithUnread> tmp_users = new LinkedList<MetaModels.UserWithUnread>();

            //iteracija niz site korisnici
            foreach (Models.User tmp in tmpUsers)
            {
                MetaModels.UserWithUnread user = new MetaModels.UserWithUnread();
                //sega go zemame brojot na neprocitani poraki i go smestuvame vo user
                user.user = tmp;
                bool at_least_one = (from last_access in repo.getMessage()
                                     where last_access.userID == userID
                                     && last_access.userID2 == tmp.id
                                     select last_access).Any();

                if (at_least_one)
                {
                    var last_check = (from access in repo.getLastOpenedMessage()
                                             where
                                             access.userId1 == userID
                                             &&
                                             access.userId2 == tmp.id
                                        select access.when);
                    if (last_check.Any())
                    {
                        var last_check_when = last_check.Max();
                        user.unread = (from message in repo.getMessage()
                                       where
                                           //gledam za so koj juzer
                                        message.userID == tmp.id

                                           //od koj juzer (last access)
                                        && message.userID2 == userID

                                           //kade vremeto na pristignuvanje e pogolemo
                                           //od moe posledno provereno
                                        && message.when > last_check_when
                                       select message).Count();
                    }
                    else
                    {
                        user.unread = (from message in repo.getMessage()
                                       where
                                           //gledam za so koj juzer
                                        message.userID == tmp.id
                                           //od koj juzer (last access)
                                        && message.userID2 == userID
                                       select message).Count();
                    }
                }
                else
                {
                    user.unread = (from message in repo.getMessage()
                                   where message.userID == tmp.id
                                   && message.userID2 == userID
                                   select message).Count();
                }
                               

                //bidejki se podredeni, dokolku se smeni godinata pri listanjeto znaci stanuva zbor
                //za student od druga generacija i go pomestuvame vo druga lista

                //ako e ista godinata dodaj go vo privremenata lista so studenti od tmp.year_attending
                if (tmp_year == tmp.year_attending)
                    tmp_users.AddLast(user);

                //ako ne togas obnovi ja listata so privremeni user-i i smeni ja godinata na generacijata,
                //dodaj gi prethodnite vo glavnata link lista od link listi
                else
                {
                    users.AddLast(tmp_users);
                    tmp_users = new LinkedList<MetaModels.UserWithUnread>();
                    tmp_year = tmp.year_attending;
                    tmp_users.AddLast(user);
                }
            }
            //poslednata link lista od studenti (t.e. studenti od poslednata generacija
            users.AddLast(tmp_users);
            //zamena za garbage collector
            tmp_users = new LinkedList<MetaModels.UserWithUnread>();

            return users;
        }
        public static String[] checkTime(int from_user, int to_user)
        {
            var all =  (from tmp in Shared.repo.getMessage()
                    where tmp.userID == from_user && tmp.userID2 == to_user
                    orderby tmp.when ascending
                    select tmp.when);
            if (all.Any())
            {
                return new String[]{all.Last().ToString()};
            }
            else
            {
                return new String[]{"never"};
            }
        }

        public static Models.User getUserByID(int id){
            return (from tmp in repo.getUsers()
                    where tmp.id == id
                    select tmp).First();
        }
    }
}