﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using alumni;

namespace alumni.Controllers
{
    public class UserController : Controller
    {
        public repository.FakeRepository repo = Shared.repo;
     
        public MetaModels.UserRequest genUserRequest(int? x) //ako x e nula znaci nema selektiran user
        {

            MetaModels.UserRequest ret = new MetaModels.UserRequest();
            List<Models.User> llu = new List<Models.User>();
            int logged_in_user_id = ((Models.User)Session["loggedInUser"]).id;
            int selected_user_id = x ?? 0; //userot so go selektirame
            
            ret.loggedIn = (from tmp in repo.getUsers()
                            where (tmp.id == logged_in_user_id)
                            select tmp).ToArray().First();
            ret.users = Shared.getUsersByGeneration();
                if (x != 0)
                {
                    var lastCVno = (from tmp in repo.getHistory_CV()
                                    where tmp.UserID == selected_user_id
                                    select tmp.ID).Max();
                    int noLastCV = Convert.ToInt32(lastCVno.ToString());

                    ret.user = (from tmp in repo.getUsers()
                                where (tmp.id == selected_user_id)
                                select tmp).ToArray().First();
                    ret.placeOfBirth = (from tmp in repo.getHistory_CV()
                                        where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                        select tmp.place_of_birth).ToArray().First();
                    ret.dateOfBirth = (DateTime)(from tmp in repo.getHistory_CV()
                                                 where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                                 select tmp.date_of_birth).ToArray().First();
                    ret.address = (from tmp in repo.getHistory_CV()
                                   where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                   select tmp.address).ToArray().First();
                    ret.phone = (from tmp in repo.getHistory_CV()
                                 where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                 select tmp.phone).ToArray().First();
                    ret.email = (from tmp in repo.getHistory_CV()
                                 where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                 select tmp.email).ToArray().First();
                    ret.martialStatus = (from tmp in repo.getHistory_CV()
                                         join tmp1 in repo.getMartialStatusOption()
                                          on tmp.Martial_status_optionID equals tmp1.ID
                                         where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                         select tmp1).ToArray().First();
                    ret.nationality = (from tmp in repo.getHistory_CV()
                                       join tmp1 in repo.getNationalityOption()
                                       on tmp.Nationality_optionaID equals tmp1.ID
                                       where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                       select tmp1).ToArray().First();
                    ret.characteristics = (from tmp in repo.getPersonalCharacteristic()
                                           where (tmp.History_CVID == selected_user_id)
                                           select tmp).ToList();
                    ret.courses = (from tmp in repo.getCourse()
                                   where (tmp.History_CVID == selected_user_id)
                                   orderby tmp.from_date ascending
                                   select tmp).ToList();
                    ret.achievements = (from tmp in repo.getAchievement()
                                        where (tmp.History_CVID == selected_user_id)
                                        select tmp).ToList();

                    ret.interests = (from temp in
                                         (from tmp in repo.getInteres()
                                          join tmp1 in repo.getInteres_Type()
                                          on tmp.Interes_TypeID equals tmp1.ID
                                          select new { tmp.History_CVID, tmp.description, tmp1.title })
                                     join temp1 in repo.getHistory_CV()
                                     on temp.History_CVID equals temp1.ID
                                     where temp1.UserID == selected_user_id && temp1.ID == noLastCV
                                     select new MetaModels.Interes(temp.description, temp.title)).ToList();
                    ret.skills = (from temp in
                                      (from tmp in repo.getSkill()
                                       join tmp1 in repo.getSkill_Type()
                                       on tmp.Skill_TypeID equals tmp1.ID
                                       select new { tmp.History_CVID, tmp.description, tmp1.title })
                                  join temp1 in repo.getHistory_CV()
                                  on temp.History_CVID equals temp1.ID
                                  where temp1.UserID == selected_user_id && temp1.ID == noLastCV
                                  select new MetaModels.Skill(temp.description, temp.title)).ToList();
                    ret.workInstitutions = (from temp in
                                                (from tmp in repo.getHistoryCVWorkInstitution()
                                                 join tmp1 in repo.getWorkInstitution()
                                                 on tmp.Work_institutionID equals tmp1.ID
                                                 select new { tmp.History_CVID, tmp.from_date, tmp.to, tmp1.title })
                                            join temp1 in repo.getHistory_CV()
                                            on temp.History_CVID equals temp1.ID
                                            where temp1.UserID == selected_user_id && temp1.ID == noLastCV
                                            select new MetaModels.HistoryCVWorkInstitution(temp.title, temp.from_date, temp.to)).ToList();
                    ret.categoriesInCV = (from temp in
                                              (from tmp in repo.getCategories_in_CV()
                                               join tmp1 in repo.getGroup_Types()
                                               on tmp.Group_TypeID equals tmp1.ID
                                               //where tmp.subscribed==true//ako sakame da gi listame site so TRUE togas UNCOMMENT
                                               select new { tmp.History_CVID, tmp.subscribed, tmp1.name })
                                          join temp1 in repo.getHistory_CV()
                                          on temp.History_CVID equals temp1.ID
                                          where temp1.UserID == selected_user_id && temp1.ID == noLastCV
                                          select new MetaModels.CategoriesInCV(temp.name, temp.subscribed)).ToList();
                    //subquerys
                    //tip so institucija
                    var x1 = (from insType in repo.getEducationType()
                              join ins in repo.getEducationInstitution()
                              on insType.ID equals ins.Education_TypeID
                              select new { ins.ID, ins.title, insType.text });
                    //povrzano so HISTORY CV EDU INSTITUTION
                    var x2 = (from tmp in x1
                              join histCVEduInst in repo.getHistoryCVEducationInstitution()
                              on tmp.ID equals histCVEduInst.Education_institutionID
                              select new { histCVEduInst.History_CVID, tmp.title, tmp.text, histCVEduInst.description, histCVEduInst.from_date, histCVEduInst.to });
                    var x3 = (from tmp in x2
                              join histCV in repo.getHistory_CV()
                              on tmp.History_CVID equals histCV.ID
                              where histCV.ID == noLastCV
                              select new { tmp.History_CVID, tmp.title, tmp.text, tmp.description, tmp.from_date, tmp.to });

                    ret.educationInstitutions = (from tmp in x3
                                                 join tmp1 in repo.getHistory_CV()
                                                 on tmp.History_CVID equals tmp1.ID
                                                 where tmp1.UserID == selected_user_id && tmp1.ID == noLastCV
                                                 orderby tmp.from_date ascending
                                                 select new MetaModels.HistoryCVEducationInstitution(new MetaModels.EducationInstitution(tmp.title, tmp.text), tmp.description, tmp.from_date, tmp.to)).ToList();

                    var q3 = (from userProj in repo.getUser_Project()
                              join proj in repo.getProject()
                              on userProj.ProjectID equals proj.ID
                              where userProj.UserID == selected_user_id && proj.History_CVID == noLastCV
                              select userProj.ProjectID);

                    ret.projects = new List<MetaModels.Project>();

                    foreach (int i in q3)
                    {
                        ret.projects.Add(new MetaModels.Project((from users in repo.getUsers()
                                                                 join id_s in
                                                                     (from userProj in repo.getUser_Project()
                                                                      where userProj.ProjectID == i
                                                                      select userProj.UserID)
                                                                 on users.id equals id_s
                                                                 select users).ToList(),
                                                                 (from up in repo.getUser_Project()
                                                                  join desc in repo.getDescription()
                                                                  on up.ID equals desc.User_ProjectID
                                                                  where up.UserID == selected_user_id
                                                                  && up.ProjectID == i
                                                                  select desc.description).First(),
                                                                  (from title in repo.getProject()
                                                                   where title.ID == i
                                                                   select title.title
                                                                    ).First()
                                                                  ));
                    }
                }
            return ret;
        }
        [HttpGet]
        public ActionResult Index(int? x)
        {
            if (Session["loggedInUser"] == null)
            {
                return Redirect("Login");
            }

            //ako x e nula znaci nema selektiran juzer
            return View(genUserRequest((x == null) ? 0 : x));
        }

        [ActionName("Index")]
        [HttpPost]
        public ActionResult Index2(MetaModels.ChatResponse chatResponse)
        {
            if (Session["loggedInUser"] == null)
            {
                return Redirect("Login");
            }

            if (chatResponse.action == 1)
                repo.retMessages.AddLast(new Models.Message(2, chatResponse.withUser, DateTime.Now, chatResponse.content));

            return View("Index", genUserRequest(chatResponse.withUser));
        }

    }
}
