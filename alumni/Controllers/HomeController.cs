﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using alumni.repository;

namespace alumni.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";
            return View();
        }

        public ActionResult About()
        {
            LinkedList<Int32> a = new LinkedList<Int32>();
            a.AddFirst(2);
            a.AddFirst(3);
            return View(a);
        }
    }
}
