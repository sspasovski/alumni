﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using alumni;

namespace alumni.Controllers
{
    public class ChatController : Controller
    {

        public repository.FakeRepository repo = Shared.repo;


        public MetaModels.ChatRequest genChatRequest(int? x){
            //objektot shto go vrakjame
            MetaModels.ChatRequest ret = new MetaModels.ChatRequest();

     /*       //momentalno logiran juzer
            Models.User userLogedIn = (from tmp in Shared.repo.getUsers()
                                          where tmp.id == 2 //ova treba da se smeni so citanje od sesija
                                          select tmp).ToArray().First();
            ret.loggedInUser = userLogedIn;*/

            Models.User userLogedIn = ret.loggedInUser = (Models.User)Session["loggedInUser"];

            //dodadi vo baza deka sme otvorale
            if (x != null)
            {
                repo.retLastOpenedMessages.AddLast(new Models.LastOpenedMessage(((Models.User)Session["loggedInUser"]).id, (int)x, DateTime.Now));
            }

            //site
            ret.users = Shared.getUsersWithUnreadByGeneration(userLogedIn.id);
            if (x != null)
            {
                if (x != 0)
                {
                    ret.withUser = (from tmp in Shared.repo.getUsers()
                                    where tmp.id == x //ova treba da se smeni so citanje od sesija
                                    select tmp).ToArray().First();
                    ret.time = Shared.checkTime(ret.withUser.id,userLogedIn.id)[0].ToString();
                }
            }
            //cashing the table
            var all_Users = (from tmp in repo.getUsers() select tmp).AsEnumerable();

            var poraki_so_id= (from tmp in repo.getMessage()
                          where (tmp.userID == userLogedIn.id && tmp.userID2 == x)
                          || (tmp.userID2 == userLogedIn.id && tmp.userID == x)
                          orderby tmp.when descending
                          select tmp);

            var poraki_edno_id = (from message in
                         poraki_so_id
                     join user1 in all_Users
                     on message.userID equals user1.id
                     select new { message, user1 });

           ret.messages  = (
                from tmp1 in
                    poraki_edno_id
                join user2 in all_Users
                on tmp1.message.userID2 equals user2.id
                select
                new MetaModels.Message(tmp1.user1, user2, tmp1.message.when,tmp1.message.content)).ToList();
            return ret;
        }

        [HttpGet]
        public ActionResult Index(int? x)
        {
            if (Session["loggedInUser"] == null)
            {
                return Redirect("Login");
            }
            //ako x e nula znaci nema selektiran juzer
            return View(genChatRequest((x == null)?0:x));
        }


        [ActionName("Index")]
        [HttpPost]
        public ActionResult Index2(MetaModels.ChatResponse chatResponse)
        {
            if (chatResponse.action == 1)
            {
                repo.addMessage(new Models.Message(((Models.User)Session["loggedInUser"]).id, chatResponse.withUser, DateTime.Now, chatResponse.content));
                repo.addLastOpenedMessage(new Models.LastOpenedMessage(((Models.User)Session["loggedInUser"]).id, chatResponse.withUser, DateTime.Now));
            }

            return View("Index",genChatRequest(chatResponse.withUser));
        }



    }
}
