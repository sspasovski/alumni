﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace alumni.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        [HttpGet]
        public ActionResult Index()
        {
            return View(Shared.getUsersByGeneration());
        }

        [HttpPost]
        public ActionResult Index(int userID)
        {
            Session["loggedInUser"] = Shared.getUserByID(userID);
            return Redirect("Wall");
        }

    }
}
