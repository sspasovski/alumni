﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using alumni;

namespace alumni.Controllers
{
    public class ProfileController : Controller
    {
        public repository.FakeRepository repo = Shared.repo;
        public MetaModels.HisCV ret = new MetaModels.HisCV();
        List<Models.User> llu = new List<Models.User>();

        

        public ActionResult Index()
        {
            if (Session["loggedInUser"] == null)
            {
                return Redirect("Login");
            }
            int logged_in_user_id = ((Models.User)Session["loggedInUser"]).id; // se cita od sesija
            var lastCVno = (from tmp in repo.getHistory_CV()
                            where tmp.UserID == logged_in_user_id
                            select tmp.ID).Max();
            int noLastCV = Convert.ToInt32(lastCVno.ToString());

            ret.user = (from tmp in repo.getUsers()
                        where (tmp.id == logged_in_user_id)
                        select tmp).ToArray().First();
            ret.placeOfBirth = (from tmp in repo.getHistory_CV()
                                where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                select tmp.place_of_birth).ToArray().First();
            ret.dateOfBirth = (DateTime)(from tmp in repo.getHistory_CV()
                                         where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                         select tmp.date_of_birth).ToArray().First();
            ret.address = (from tmp in repo.getHistory_CV()
                           where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                           select tmp.address).ToArray().First();
            ret.phone = (from tmp in repo.getHistory_CV()
                         where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                         select tmp.phone).ToArray().First();
            ret.email = (from tmp in repo.getHistory_CV()
                         where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                         select tmp.email).ToArray().First();
            ret.martialStatus = (from tmp in repo.getHistory_CV()
                                 join tmp1 in repo.getMartialStatusOption()
                                  on tmp.Martial_status_optionID equals tmp1.ID
                                 where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                 select tmp1).ToArray().First();
            ret.nationality = (from tmp in repo.getHistory_CV()
                               join tmp1 in repo.getNationalityOption()
                               on tmp.Nationality_optionaID equals tmp1.ID
                               where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                               select tmp1).ToArray().First();
            ret.characteristics = (from tmp in repo.getPersonalCharacteristic()
                                   where (tmp.History_CVID == noLastCV)
                                   select tmp).ToList();
            ret.courses = (from tmp in repo.getCourse()
                           where (tmp.History_CVID == noLastCV)
                           orderby tmp.from_date ascending
                           select tmp).ToList();
            ret.achievements = (from tmp in repo.getAchievement()
                                where (tmp.History_CVID == noLastCV)
                                select tmp).ToList();

            ret.interests = (from temp in
                                 (from tmp in repo.getInteres()
                                  join tmp1 in repo.getInteres_Type()
                                  on tmp.Interes_TypeID equals tmp1.ID
                                  select new { tmp.History_CVID, tmp.description, tmp1.title })
                             join temp1 in repo.getHistory_CV()
                             on temp.History_CVID equals temp1.ID
                             where temp1.UserID == logged_in_user_id && temp1.ID == noLastCV
                             select new MetaModels.Interes(temp.description, temp.title)).ToList();
            ret.skills = (from temp in
                              (from tmp in repo.getSkill()
                               join tmp1 in repo.getSkill_Type()
                               on tmp.Skill_TypeID equals tmp1.ID
                               select new { tmp.History_CVID, tmp.description, tmp1.title })
                          join temp1 in repo.getHistory_CV()
                          on temp.History_CVID equals temp1.ID
                          where temp1.UserID == logged_in_user_id && temp1.ID == noLastCV
                          select new MetaModels.Skill(temp.description, temp.title)).ToList();
            ret.workInstitutions = (from temp in
                                        (from tmp in repo.getHistoryCVWorkInstitution()
                                         join tmp1 in repo.getWorkInstitution()
                                         on tmp.Work_institutionID equals tmp1.ID
                                         select new { tmp.History_CVID, tmp.from_date, tmp.to, tmp1.title })
                                    join temp1 in repo.getHistory_CV()
                                    on temp.History_CVID equals temp1.ID
                                    where temp1.UserID == logged_in_user_id && temp1.ID == noLastCV
                                    select new MetaModels.HistoryCVWorkInstitution(temp.title, temp.from_date, temp.to)).ToList();
            ret.categoriesInCV = (from temp in
                                      (from tmp in repo.getCategories_in_CV()
                                       join tmp1 in repo.getGroup_Types()
                                       on tmp.Group_TypeID equals tmp1.ID
                                       //where tmp.subscribed==true//ako sakame da gi listame site so TRUE togas UNCOMMENT
                                       select new { tmp.History_CVID, tmp.subscribed, tmp1.name })
                                  join temp1 in repo.getHistory_CV()
                                  on temp.History_CVID equals temp1.ID
                                  where temp1.UserID == logged_in_user_id && temp1.ID == noLastCV
                                  select new MetaModels.CategoriesInCV(temp.name, temp.subscribed)).ToList();
            //subquerys
            //tip so institucija
            var x1 = (from insType in repo.getEducationType()
                      join ins in repo.getEducationInstitution()
                      on insType.ID equals ins.Education_TypeID
                      select new { ins.ID, ins.title, insType.text });
            //povrzano so HISTORY CV EDU INSTITUTION
            var x2 = (from tmp in x1
                      join histCVEduInst in repo.getHistoryCVEducationInstitution()
                      on tmp.ID equals histCVEduInst.Education_institutionID
                      select new { histCVEduInst.History_CVID, tmp.title, tmp.text, histCVEduInst.description, histCVEduInst.from_date, histCVEduInst.to });
            var x3 = (from tmp in x2
                      join histCV in repo.getHistory_CV()
                      on tmp.History_CVID equals histCV.ID
                      where histCV.ID == noLastCV
                      select new { tmp.History_CVID, tmp.title, tmp.text, tmp.description, tmp.from_date, tmp.to });

            ret.educationInstitutions = (from tmp in x3
                                         join tmp1 in repo.getHistory_CV()
                                         on tmp.History_CVID equals tmp1.ID
                                         where tmp1.UserID == logged_in_user_id && tmp1.ID == noLastCV
                                         orderby tmp.from_date ascending
                                         select new MetaModels.HistoryCVEducationInstitution(new MetaModels.EducationInstitution(tmp.title, tmp.text), tmp.description, tmp.from_date, tmp.to)).ToList();

            var q3 = (from userProj in repo.getUser_Project()
                      join proj in repo.getProject()
                      on userProj.ProjectID equals proj.ID
                      where userProj.UserID == logged_in_user_id && proj.History_CVID == noLastCV
                      select userProj.ProjectID);

            ret.projects = new List<MetaModels.Project>();

            foreach (int i in q3)
            {
                ret.projects.Add(new MetaModels.Project((from users in repo.getUsers()
                                                         join id_s in
                                                             (from userProj in repo.getUser_Project()
                                                              where userProj.ProjectID == i
                                                              select userProj.UserID)
                                                         on users.id equals id_s
                                                         select users).ToList(),
                                                         (from up in repo.getUser_Project()
                                                          join desc in repo.getDescription()
                                                          on up.ID equals desc.User_ProjectID
                                                          where up.UserID == logged_in_user_id
                                                          && up.ProjectID == i
                                                          select desc.description).First(),
                                                          (from title in repo.getProject()
                                                           where title.ID == i
                                                           select title.title
                                                            ).First()
                                                          ));
            }
            return View(ret);
        }

        [HttpPost]
        public ActionResult EditCV(alumni.MetaModels.HiSCVResponse resp)
        {
            //menuvanje juzer
            int logged_in_user_id = ((Models.User)Session["loggedInUser"]).id; // se cita od sesija
            alumni.Models.User logged_in_user = (from tmp in repo.getUsers()
                        where (tmp.id == logged_in_user_id)
                        select tmp).ToArray().First();

            alumni.Models.User user = new Models.User(logged_in_user_id, resp.name, resp.surname, resp.gender, logged_in_user.year_attending, logged_in_user.year_graduation, resp.picture_url);
            repo.updateUser(logged_in_user_id, user);

            //nov History CV ID
            alumni.Models.History_CV hisCV = new Models.History_CV();
            hisCV.UserID = logged_in_user_id;
            string[] date_parts = resp.dateOfBirth.Split('.');
            hisCV.date_of_birth = new DateTime(Convert.ToInt32(date_parts[2]), Convert.ToInt32(date_parts[1]), Convert.ToInt32(date_parts[0]));
            hisCV.email = resp.email;
            hisCV.Martial_status_optionID = resp.martial;
            int nat_id = Convert.ToInt32(resp.nationality.Split('^')[0]);
            if (nat_id == 0)
            {
                hisCV.Nationality_optionaID=repo.addNationality_option(new Models.Nationality_option(0, resp.nationality.Split('^')[1]));
            }
            else
            {
                hisCV.Nationality_optionaID = nat_id;
            }
            hisCV.phone = resp.phone;
            hisCV.place_of_birth = resp.placeOfBirth;
            int hisCV_ID = repo.addHistory_CV(hisCV);

            //achievements
            string [] achievemts_name = resp.achievements.Split('|')[0].Split('#');
            DateTime[] achievemts_date = new DateTime[achievemts_name.Length];
            for(int i=1; i<achievemts_date.Length; i++)
            {
                string [] str_date= resp.achievements.Split('|')[1].Split('#')[i].Split('.');
                achievemts_date[i]=new DateTime(Convert.ToInt32(str_date[2]), Convert.ToInt32(str_date[1]), Convert.ToInt32(str_date[0]));
            }
            string [] achievemts_desc = resp.achievements.Split('|')[2].Split('#');

            for (int i = 1; i < achievemts_date.Length; i++)
            {
                repo.addAchievement(new Models.Achievement(0,hisCV_ID,achievemts_name[i],achievemts_desc[i],achievemts_date[i]));
            }


            //characteristicks
            int c=0;
            foreach (string characteristic in resp.characteristics.Split('#'))
            {
                if (c++ == 0)
                    continue;
                repo.addPersonal_Characteristic(new Models.Personal_Characteristic(hisCV_ID,c++,characteristic));
            }

            //courses
            string[] courses_names = resp.courses.Split('|')[0].Split('#');
            string[] courses_desc = resp.courses.Split('|')[1].Split('#');
            DateTime[] courses_from = new DateTime[courses_names.Length];
            for (int i = 1; i < courses_names.Length; i++)
            {
                string[] str_date = resp.courses.Split('|')[2].Split('#')[i].Split('.');
                courses_from[i] = new DateTime(Convert.ToInt32(str_date[2]), Convert.ToInt32(str_date[1]), Convert.ToInt32(str_date[0]));
            }
            DateTime?[] courses_to = new DateTime?[courses_names.Length];
            for (int i = 1; i < courses_names.Length; i++)
            {
                string[] str_date = resp.courses.Split('|')[3].Split('#')[i].Split('.');
                if (str_date.Length == 3)
                {
                    courses_to[i] = new DateTime(Convert.ToInt32(str_date[2]), Convert.ToInt32(str_date[1]), Convert.ToInt32(str_date[0]));
                }
            }

            for (int i = 1; i < courses_names.Length; i++)
            {
                repo.addCourse(new Models.Course(0,hisCV_ID,courses_names[i],courses_desc[i],courses_from[i],courses_to[i]));
            }
            

            //education institutions
            
            string[] edu_str_id = resp.educationInstitutions.Split('|')[0].Split('#');
            int edu_len = edu_str_id.Length;
            int[] edu_type_id = new int[edu_len];
            for (int i = 1; i < edu_len; i++)
            {
                edu_type_id[i] = Convert.ToInt32(edu_str_id[i]);
            }

            int[] edu_ins_id = new int[edu_len];
            string[] edu_ins_title = new string[edu_len];
            for (int i = 1; i < edu_len; i++)
            {
                edu_ins_id[i] = Convert.ToInt32(resp.educationInstitutions.Split('|')[1].Split('#')[i].Split('^')[0]);
                edu_ins_title[i] = resp.educationInstitutions.Split('|')[1].Split('#')[i].Split('^')[1];
            }

            DateTime[] edu_inst_from = new DateTime[edu_len];
            for (int i = 1; i < edu_len; i++)
            {
                string[] str_date = resp.educationInstitutions.Split('|')[2].Split('#')[i].Split('.');
                edu_inst_from[i] = new DateTime(Convert.ToInt32(str_date[2]), Convert.ToInt32(str_date[1]), Convert.ToInt32(str_date[0]));
            }
            DateTime?[] edu_inst_to = new DateTime?[edu_len];
            for (int i = 1; i < edu_len; i++)
            {
                string[] str_date = resp.educationInstitutions.Split('|')[3].Split('#')[i].Split('.');
                if (str_date.Length == 3)
                {
                    edu_inst_to[i] = new DateTime(Convert.ToInt32(str_date[2]), Convert.ToInt32(str_date[1]), Convert.ToInt32(str_date[0]));
                }
            }
            string[] edu_ins_desc = resp.educationInstitutions.Split('|')[4].Split('#');

            for (int i = 1; i < edu_len; i++)
            {
                if (edu_ins_id[i] == 0)
                {
                    int new_edu_inst_id = repo.addEducation_institution(new Models.Education_institution(0,edu_ins_title[i],edu_type_id[i]));
                    edu_ins_id[i] = new_edu_inst_id;
                }
                repo.addHistory_CV_Education_institution(new Models.History_CV_Education_institution(hisCV_ID,edu_ins_id[i],edu_ins_desc[i],edu_inst_from[i],edu_inst_to[i]));
            }


            //interests
            string[] interest_id_and_title = resp.interests.Split('|')[0].Split('#');
            int inter_len = interest_id_and_title.Length;
            string[] interest_desc = resp.interests.Split('|')[1].Split('#');
            for (int i = 1; i < inter_len; i++)
            {
                int inter_tmp_id = Convert.ToInt32(interest_id_and_title[i].Split('^')[0]);
                if(inter_tmp_id==0){
                    inter_tmp_id=repo.addInteres_Type(new Models.Interes_Type(0,interest_id_and_title[i].Split('^')[1]));
                }
                repo.addInteres(new Models.Interes(hisCV_ID, inter_tmp_id, interest_desc[i]));
            }

            //PROEKTIIIIIIIIIIIIIIIIIIIIIIII
            string [] proj_all = resp.project.Split('|');
            int proj_len = proj_all[0].Split('#').Length;
            string[] proj_names = proj_all[0].Split('#');
            string[] proj_desc = proj_all[1].Split('#');
            string[] proj_title_user = proj_all[2].Split('#');
            
            int proj_ttl_users=proj_title_user.Length;
            string[] proj_title = new string[proj_ttl_users];
            int[] proj_user = new int[proj_ttl_users];
            for (int i = 1; i < proj_ttl_users; i++)
            {
                proj_title[i] = proj_title_user[i].Split('^')[0];
                proj_user[i] = Convert.ToInt32(proj_title_user[i].Split('^')[1]);
            }

            c = 1;
            for (int i = 1; i < proj_len; i++)
            {
                int proj_tmp_id = repo.addProject(new Models.Project(0, hisCV_ID, proj_names[i]));
                string proj_tmp_title = proj_title[c];
                while (proj_tmp_title == proj_title[c])
                {
                    repo.addUser_Project(new Models.User_Project(0, proj_user[c], proj_tmp_id));
                    c++;
                    if (c >= proj_ttl_users)
                        break;
                }
                int user_proj_id = repo.addUser_Project(new Models.User_Project(0, logged_in_user_id, proj_tmp_id));
                repo.addDescription(new Models.Description(user_proj_id, proj_desc[i]));
            }

            //skills
            string[] skill_id_and_title = resp.skills.Split('|')[0].Split('#');
            int skill_len = skill_id_and_title.Length;
            string[] skill_desc = resp.skills.Split('|')[1].Split('#');
            for (int i = 1; i < skill_len; i++)
            {
                int skill_tmp_id = Convert.ToInt32(skill_id_and_title[i].Split('^')[0]);
                if (skill_tmp_id == 0)
                {
                    skill_tmp_id = repo.addSkill_Type(new Models.Skill_Type(0, skill_id_and_title[i].Split('^')[1]));
                }
                repo.addSkill(new Models.Skill(hisCV_ID, skill_tmp_id, skill_desc[i]));
            }



            //Work work, ready to work
            int work_len = resp.work.Split('|')[0].Split('#').Length;
            int[] work_ins_id = new int[work_len];
            string[] work_ins_title = new string[work_len];
            for (int i = 1; i < work_len; i++)
            {
                work_ins_id[i] = Convert.ToInt32(resp.work.Split('|')[0].Split('#')[i].Split('^')[0]);
                work_ins_title[i] = resp.work.Split('|')[0].Split('#')[i].Split('^')[1];
            }

            DateTime[] work_inst_from = new DateTime[work_len];
            for (int i = 1; i < work_len; i++)
            {
                string[] str_date = resp.work.Split('|')[1].Split('#')[i].Split('.');
                work_inst_from[i] = new DateTime(Convert.ToInt32(str_date[2]), Convert.ToInt32(str_date[1]), Convert.ToInt32(str_date[0]));
            }
            DateTime?[] work_inst_to = new DateTime?[work_len];
            for (int i = 1; i < work_len; i++)
            {
                string[] str_date = resp.work.Split('|')[2].Split('#')[i].Split('.');
                if (str_date.Length == 3)
                {
                    work_inst_to[i] = new DateTime(Convert.ToInt32(str_date[2]), Convert.ToInt32(str_date[1]), Convert.ToInt32(str_date[0]));
                }
            }

            for (int i = 1; i < work_len; i++)
            {
                if (work_ins_id[i] == 0)
                {
                    int new_work_inst_id = repo.addWork_institution(new Models.Work_institution(0,work_ins_title[i]));
                    work_ins_id[i] = new_work_inst_id;
                }
                repo.addHistory_CV_Work_institution(new Models.History_CV_Work_institution(hisCV_ID, work_ins_id[i], work_inst_from[i], work_inst_to[i]));

            }

            return EditCV();


        }

        [HttpGet]
        public ActionResult EditCV()
        {
            if (Session["loggedInUser"] == null)
            {
                return Redirect("Login");
            }
            int logged_in_user_id = ((Models.User)Session["loggedInUser"]).id; // se cita od sesija
            var lastCVno = (from tmp in repo.getHistory_CV()
                            where tmp.UserID == logged_in_user_id
                            select tmp.ID).Max();
            int noLastCV = Convert.ToInt32(lastCVno.ToString());

            ret.user = (from tmp in repo.getUsers()
                        where (tmp.id == logged_in_user_id)
                        select tmp).ToArray().First();
            ret.placeOfBirth = (from tmp in repo.getHistory_CV()
                                where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                select tmp.place_of_birth).ToArray().First();
            ret.dateOfBirth = (DateTime)(from tmp in repo.getHistory_CV()
                                         where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                         select tmp.date_of_birth).ToArray().First();
            ret.address = (from tmp in repo.getHistory_CV()
                           where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                           select tmp.address).ToArray().First();
            ret.phone = (from tmp in repo.getHistory_CV()
                         where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                         select tmp.phone).ToArray().First();
            ret.email = (from tmp in repo.getHistory_CV()
                         where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                         select tmp.email).ToArray().First();
            ret.martialStatus = (from tmp in repo.getHistory_CV()
                                 join tmp1 in repo.getMartialStatusOption()
                                  on tmp.Martial_status_optionID equals tmp1.ID
                                 where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                                 select tmp1).ToArray().First();
            ret.nationality = (from tmp in repo.getHistory_CV()
                               join tmp1 in repo.getNationalityOption()
                               on tmp.Nationality_optionaID equals tmp1.ID
                               where (tmp.UserID == ret.user.id && tmp.ID == noLastCV)
                               select tmp1).ToArray().First();
            ret.characteristics = (from tmp in repo.getPersonalCharacteristic()
                                   where (tmp.History_CVID == noLastCV)
                                   select tmp).ToList();
            ret.courses = (from tmp in repo.getCourse()
                           where (tmp.History_CVID == noLastCV)
                           orderby tmp.from_date ascending
                           select tmp).ToList();
            ret.achievements = (from tmp in repo.getAchievement()
                                where (tmp.History_CVID == noLastCV)
                                select tmp).ToList();

            ret.interests = (from temp in
                                 (from tmp in repo.getInteres()
                                  join tmp1 in repo.getInteres_Type()
                                  on tmp.Interes_TypeID equals tmp1.ID
                                  select new { tmp.History_CVID, tmp.description, tmp1.title })
                             join temp1 in repo.getHistory_CV()
                             on temp.History_CVID equals temp1.ID
                             where temp1.UserID == logged_in_user_id && temp1.ID == noLastCV
                             select new MetaModels.Interes(temp.description, temp.title)).ToList();
            ret.skills = (from temp in
                              (from tmp in repo.getSkill()
                               join tmp1 in repo.getSkill_Type()
                               on tmp.Skill_TypeID equals tmp1.ID
                               select new { tmp.History_CVID, tmp.description, tmp1.title })
                          join temp1 in repo.getHistory_CV()
                          on temp.History_CVID equals temp1.ID
                          where temp1.UserID == logged_in_user_id && temp1.ID == noLastCV
                          select new MetaModels.Skill(temp.description, temp.title)).ToList();
            ret.workInstitutions = (from temp in
                                        (from tmp in repo.getHistoryCVWorkInstitution()
                                         join tmp1 in repo.getWorkInstitution()
                                         on tmp.Work_institutionID equals tmp1.ID
                                         select new { tmp.History_CVID, tmp.from_date, tmp.to, tmp1.title })
                                    join temp1 in repo.getHistory_CV()
                                    on temp.History_CVID equals temp1.ID
                                    where temp1.UserID == logged_in_user_id && temp1.ID == noLastCV
                                    select new MetaModels.HistoryCVWorkInstitution(temp.title, temp.from_date, temp.to)).ToList();
            ret.categoriesInCV = (from temp in
                                      (from tmp in repo.getCategories_in_CV()
                                       join tmp1 in repo.getGroup_Types()
                                       on tmp.Group_TypeID equals tmp1.ID
                                       //where tmp.subscribed==true//ako sakame da gi listame site so TRUE togas UNCOMMENT
                                       select new { tmp.History_CVID, tmp.subscribed, tmp1.name })
                                  join temp1 in repo.getHistory_CV()
                                  on temp.History_CVID equals temp1.ID
                                  where temp1.UserID == logged_in_user_id && temp1.ID == noLastCV
                                  select new MetaModels.CategoriesInCV(temp.name, temp.subscribed)).ToList();
            //subquerys
            //tip so institucija
            var x1 = (from insType in repo.getEducationType()
                      join ins in repo.getEducationInstitution()
                      on insType.ID equals ins.Education_TypeID
                      select new { ins.ID, ins.title, insType.text });
            //povrzano so HISTORY CV EDU INSTITUTION
            var x2 = (from tmp in x1
                      join histCVEduInst in repo.getHistoryCVEducationInstitution()
                      on tmp.ID equals histCVEduInst.Education_institutionID
                      select new { histCVEduInst.History_CVID, tmp.title, tmp.text, histCVEduInst.description, histCVEduInst.from_date, histCVEduInst.to });
            var x3 = (from tmp in x2
                      join histCV in repo.getHistory_CV()
                      on tmp.History_CVID equals histCV.ID
                      where histCV.ID == noLastCV
                      select new { tmp.History_CVID, tmp.title, tmp.text, tmp.description, tmp.from_date, tmp.to });

            ret.educationInstitutions = (from tmp in x3
                                         join tmp1 in repo.getHistory_CV()
                                         on tmp.History_CVID equals tmp1.ID
                                         where tmp1.UserID == logged_in_user_id && tmp1.ID == noLastCV
                                         orderby tmp.from_date ascending
                                         select new MetaModels.HistoryCVEducationInstitution(new MetaModels.EducationInstitution(tmp.title, tmp.text), tmp.description, tmp.from_date, tmp.to)).ToList();

            var q3 = (from userProj in repo.getUser_Project()
                      join proj in repo.getProject()
                      on userProj.ProjectID equals proj.ID
                      where userProj.UserID == logged_in_user_id && proj.History_CVID == noLastCV
                      select userProj.ProjectID);

            ret.projects = new List<MetaModels.Project>();

            foreach (int i in q3)
            {
                ret.projects.Add(new MetaModels.Project((from users in repo.getUsers()
                                                         join id_s in
                                                             (from userProj in repo.getUser_Project()
                                                              where userProj.ProjectID == i
                                                              select userProj.UserID)
                                                         on users.id equals id_s
                                                         select users).ToList(),
                                                         (from up in repo.getUser_Project()
                                                          join desc in repo.getDescription()
                                                          on up.ID equals desc.User_ProjectID
                                                          where up.UserID == logged_in_user_id
                                                          && up.ProjectID == i
                                                          select desc.description).First(),
                                                          (from title in repo.getProject()
                                                           where title.ID == i
                                                           select title.title
                                                            ).First()
                                                          ));
            }

            ret.nacionalnostDDL = (from tmp in repo.getNationalityOption()select tmp).ToList();
            ret.martialDDL = (from tmp in repo.getMartialStatusOption()select tmp).ToList();
            ret.EduTypeDDL = (from tmp in repo.getEducationType()select tmp).ToList();
            ret.EduInstDDL = (from tmp in repo.getEducationInstitution()select tmp).ToList();
            ret.InteresDDL = (from tmp in repo.getInteres_Type() select tmp).ToList();
            ret.SkillsDDL = (from tmp in repo.getSkill_Type() select tmp).ToList();
            ret.WorkInstDDL = (from tmp in repo.getWorkInstitution() select tmp).ToList();

            return View(ret);
        }
    }
}
