﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using alumni.repository;
using alumni;

namespace alumni.Controllers
{
    public class WallController : Controller
    {
        
        //
        // GET: /Wall/
        public FakeRepository repo = Shared.repo;

        public MetaModels.WallRequest genWallRequest(int? selected)
        {
            

            //go vrakja site grupi
            MetaModels.WallRequest request = new MetaModels.WallRequest();
            request.categories = new LinkedList<MetaModels.Category>();
            //po default selektirana e prvata kategorija
            request.choosen_category = selected ?? 1;//ova e NVL operator
            //ako selected e null togas daj mu 1

                //go zemame ONLINE korisnikot
            request.logged_in = (Models.User)Session["loggedInUser"];

            var logged_user_last_time_visited = (from tmp in repo.getLastVisited()
                                                 where tmp.userID == request.logged_in.id
                                                 select tmp).AsEnumerable();

            var all_replies = (from tmp in repo.getReplies() select tmp).AsQueryable();
            var all_users = (from tmp in repo.getUsers() select tmp).AsQueryable();

            var groups =
                (from tmp in repo.getGroup_Types() select tmp).ToList();

            //stavame deka sme ja posetile
            if (selected != null)
            {
                Models.LastVisited lv = new Models.LastVisited(request.logged_in.id, (int)selected, System.DateTime.Now);
                repo.addLastVisited(lv);
            }

            //za sekoja grupa da najdeme notifikacii
            foreach (var one_group in groups)
            {
                MetaModels.Category tmp_category = new MetaModels.Category();
                tmp_category.name = one_group.name;
                tmp_category.unread = 0;
                tmp_category.id = one_group.ID;
                tmp_category.url = one_group.url;
                bool unread_generated = false;
                if (selected != null)
                {

                    if (selected == one_group.ID)
                    {

                       

                        unread_generated = true;
                        //gi skladirame NOTIFIKACIITE vo LISTA
                        var notif =
                            (from tmp in repo.getNotifications()
                             where tmp.groupTypeId == one_group.ID
                             select tmp).ToList();

                        //za sekoja NOTIFIKACIJA gledame dali e REQUEST
                        foreach (var one_notif in notif)
                        {
                            //ako e request go zemame vo one_req
                            var req =
                                (from tmp in repo.getRequests()
                                 where tmp.NotificationID == one_notif.id
                                 select tmp);

                            MetaModels.Notification tmp_notif = new MetaModels.Notification(); ;
                            tmp_notif.id = one_notif.id;

                            //ako e request
                            if (req.Any())//proverka dali ima rezultati vo req
                            {
                                var one_req = req.ToArray().Max();

                                //site shto pochnuvaat so tmp postojat samo vo blokov
                                //i sluzat za popolnuvanje na Request
                                tmp_notif = new MetaModels.Request();
                                ((MetaModels.Request)tmp_notif).available_from = one_req.avaliable_from;
                                ((MetaModels.Request)tmp_notif).available_to = one_req.avaliable_to;
                                ((MetaModels.Request)tmp_notif).replays = new LinkedList<MetaModels.Reply>();
                                ((MetaModels.Request)tmp_notif).id = one_req.NotificationID;
                                //za sekoj REQUEST vadime LISTA od REPLAYS
                                var replayN =
                                    (from tmp in repo.getReplies()
                                     where tmp.RequestNotiﬁcationID == one_req.NotificationID
                                     select tmp).ToList();

                                //za sekoj REPLAY da vrati lista od REPLAYS
                                foreach (var one_replay in replayN)
                                {
                                    ((MetaModels.Request)tmp_notif).replays.AddLast(createMetaReply(one_replay, all_replies, all_users));
                                    /*   MetaModels.Replay tmp_replay = new MetaModels.Replay();
                                       tmp_replay.replays = getAllReplies(one_replay, all_replies, all);
                                       var replayR =
                                           (from tmp in repo.getReplies()
                                            where tmp.ReplyID == one_replay.ReplyID
                                            select tmp).ToList();*/
                                }
                            }
                            else//ako ne e request
                            {//site shto pochnuvaat so tmp postojat samo vo blokov
                                //i sluzat za popolnuvanje na Notification
                                tmp_notif = new MetaModels.Notification();

                            }
                            tmp_notif.content = one_notif.content;
                            tmp_notif.user =
                                (from tmp in repo.getUsers()
                                 where tmp.id == one_notif.userId
                                 select tmp).ToArray().First();
                            tmp_notif.when = one_notif.when;
                            tmp_notif.id = one_notif.id;

                            var last_vist = (from tmp in logged_user_last_time_visited
                                             where tmp.groupTypeID == one_group.ID
                                                && tmp.userID == request.logged_in.id
                                             select
                                                   tmp.when);
                            if (!last_vist.Any())
                            {
                                tmp_category.unread++;
                            }
                            else if (last_vist.Max() < tmp_notif.when)
                            {
                                tmp_category.unread++;
                            }

                            //notifikacijata ja dodavame na kategorijata
                            //vrz kojashto momentalno iterirame
                            tmp_category.notifications.AddLast(tmp_notif);
                        }
                    }

                }

                if (!unread_generated)
                {
                    var last_checked = (from log in logged_user_last_time_visited
                                        where log.groupTypeID == one_group.ID
                                           && log.userID == request.logged_in.id
                                        select
                                              log.when);
                    DateTime? when = (last_checked.Any()) ? last_checked.Max() : new DateTime(1990, 01, 07, 18, 00, 30);
                    when = (when == null) ? new DateTime(1990, 01, 07, 18, 00, 30) : when;
                    var notifes = (from notif in repo.getNotifications()
                                   where notif.groupTypeId == one_group.ID
                                   && notif.when > when
                                   select notif
                                               );

                    tmp_category.unread = (notifes.Any()) ? notifes.Count() : 0;

                }

                //dodavame vo lista od kategorii napolneta kategorija
                request.categories.AddLast(tmp_category);
            }
            //    request.categories.AddLast();
            return request;
        }

        //za daden odgovor od modelot
        //kreira metamodel vo kojsto vo lista gi dodava site negovi META-modeli
        public MetaModels.Reply createMetaReply(Models.Reply source, IQueryable<Models.Reply> all_R, IQueryable<Models.User> all_U){
            //go kreirame objektot
            MetaModels.Reply ret = new MetaModels.Reply();
            ret.when = source.when;
            ret.id = source.ID;
            ret.replays = new LinkedList<MetaModels.Reply>();
            ret.user = (from tmp in all_U where source.UserID == tmp.id select tmp).AsEnumerable().First();
            ret.content = (from tmp in all_R where source.ID == tmp.ID select tmp).AsEnumerable().First().content;
            //za sekoj odgovor na odgovorot, kreirame nivni objekti
            foreach (var tmp in
                            (from tmp in all_R
                            where tmp.ReplyID == source.ID
                            select tmp))
            {
                ret.replays.AddLast(createMetaReply(tmp, all_R, all_U));
                ret.replays.Last.Value.father = ret;
            }
            return ret;
        }

        [HttpGet]
        public ActionResult Index(int? selected)
        {
            if (Session["loggedInUser"] == null)
            {
                return Redirect("Login");
            }

            return View(genWallRequest(selected));
        }

        [HttpPost]
        public ActionResult Index(MetaModels.WallResponse arg)
        {
            if (Session["loggedInUser"] == null)
            {
                return Redirect("Login");
            }
            Dictionary<string, Int32> months = new Dictionary<string, Int32>();
            months.Add("Јануари", 1);
            months.Add("Февруари", 2);
            months.Add("Март", 3);
            months.Add("Април", 4);
            months.Add("Мај", 5);
            months.Add("Јуни", 6);
            months.Add("Јули", 7);
            months.Add("Август", 8);
            months.Add("Септември", 9);
            months.Add("Октомври", 10);
            months.Add("Ноември", 11);
            months.Add("Декември", 12);
            switch(arg.action)
            {
                case 0:
                    alumni.Models.Notification notif = new Models.Notification();
                    notif.content = arg.notif_content;
                    notif.groupTypeId = arg.choosen_category;
                    notif.userId = arg.user_id;
                    repo.addNotification(notif);

                return View(genWallRequest(arg.choosen_category));

                case 1:
                    alumni.Models.Notification notif_req = new Models.Notification();
                    notif_req.content = arg.notif_content;
                    notif_req.groupTypeId = arg.choosen_category;
                    notif_req.userId = arg.user_id;
                    alumni.Models.Request req = new Models.Request();
                    req.NotificationID = repo.addNotification(notif_req);
                    string [] dates=arg.from_to.Split('÷');
                    string[] pargs = dates[0].Split(' ');
                    Int32 month=0;
                    String month_s = pargs[1].Remove(pargs[1].Length-1,1);
                    months.TryGetValue(month_s, out month);
                    req.avaliable_from = new DateTime(Convert.ToInt32(pargs[2]),month,Convert.ToInt32(pargs[0]));

                    pargs = dates[1].Split(' ');
                    month=0;
                    month_s = pargs[2].Remove(pargs[2].Length - 1, 1);
                    months.TryGetValue(month_s, out month);
                    req.avaliable_to = new DateTime(Convert.ToInt32(pargs[3]),month,Convert.ToInt32(pargs[1]));
                    repo.addRequest(req);
                return View(genWallRequest(arg.choosen_category));


                case 2:
                alumni.Models.Reply added2 = new alumni.Models.Reply(arg.response_to_req_id, null , arg.user_id, arg.response_text, DateTime.Now);
                repo.addReply(added2);
                return View(genWallRequest(arg.choosen_category));

                case 3:
               //     repo.addReply(
                alumni.Models.Reply added = new alumni.Models.Reply(null, arg.response_to_req_id, arg.user_id, arg.response_text, DateTime.Now);
                repo.addReply(added);
                return View(genWallRequest(arg.choosen_category));

                
                case 5:
                    repo.removeRequest(arg.delete_id);
                    return View(genWallRequest(arg.choosen_category));

                case 6:
                    repo.removeReply(arg.delete_id);
                    return View(genWallRequest(arg.choosen_category));

                case 7:
                    repo.removeNotification(arg.delete_id);
                    return View(genWallRequest(arg.choosen_category));

                case 4:
                return View(genWallRequest(arg.choosen_category));

                default:
                return View();
            }
        }
    }
}
