﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using alumni.Models;

namespace alumni.repository
{
    public class FakeRepository 
    {
        public LinkedList<Message> retMessages { get; set; }
        public LinkedList<Reply> retReplay { get; set; }
        public LinkedList<Group_Type> retGroupTypes { get; set; }
        public LinkedList<User> retUsers { get; set; }
        public LinkedList<Notification> retNotifications { get; set; }
        public LinkedList<LastOpenedMessage> retLastOpenedMessages { get; set; }
        public LinkedList<LastVisited> retLastVisited { get; set; }
        public LinkedList<Request> retRequests { get; set; }
        public LinkedList<Categories_in_CV> retCategoriesInCV { get; set; }
        public LinkedList<History_CV> retHistoryCV { get; set; }
        public LinkedList<Project> retProject { get; set; }
        public LinkedList<User_Project> retUserProject { get; set; }
        public LinkedList<Description> retDescription { get; set; }
        public LinkedList<Interes> retInteres { get; set; }
        public LinkedList<Interes_Type> retInteresType { get; set; }
        public LinkedList<Skill> retSkill { get; set; }
        public LinkedList<Skill_Type> retSkillType { get; set; }
        public LinkedList<Martial_status_option> retMartialStatusOption { get; set; }
        public LinkedList<Nationality_option> retNationalityOption { get; set; }
        public LinkedList<History_CV_Education_institution> retHistoryCVEducationInstitution { get; set; }
        public LinkedList<Education_institution> retEducationInstitution { get; set; }
        public LinkedList<Education_Type> retEducationType { get; set; }
        public LinkedList<History_CV_Work_institution> retHistoryCVWorkInstitution { get; set; }
        public LinkedList<Work_institution> retWorkInstitution { get; set; }
        public LinkedList<Personal_Characteristic> retPersonalCharacteristic { get; set; }
        public LinkedList<Course> retCourse { get; set; }
        public LinkedList<Achievement> retAchievement { get; set; }

        public void addMessage(Message arg)
        {
            arg.when = DateTime.Now;
            retMessages.AddLast(arg);
        }
        public int addNotification(Notification arg)
        {
            arg.when = DateTime.Now;
            int id = 0;
            foreach (var tmp in getNotifications())
            {
                if (tmp.id > id)
                    id = tmp.id;
            }
            arg.id = id + 1;
            retNotifications.AddLast(arg);
            return arg.id;
        }
        public void addRequest(Request arg)
        {
            retRequests.AddLast(arg);
        }

        public void removeNotification(int arg)
        {
            Notification za_brishenje = new Notification();
            foreach (Notification tmp in retNotifications)
            {
                if (tmp.id == arg)
                {
                    za_brishenje = tmp;
                }
            }
            retNotifications.Remove(za_brishenje);
        }

        public void removeRequest(int arg)
        {
            Request za_brishenje = new Request();
            foreach (Request tmp in retRequests)
            {
                if (tmp.NotificationID == arg)
                {
                    za_brishenje = tmp;
                }
            }
            retRequests.Remove(za_brishenje);
            removeNotification(arg);
        }
        public void removeReply(int arg)
        {
            Reply za_brisenje = new Reply();
            foreach (Reply tmp in retReplay)
            {
                if (tmp.ID == arg)
                {
                    za_brisenje = tmp;
                  
                }
            }
            retReplay.Remove(za_brisenje);
        }

        public int addReply(Reply arg)
        {
            int id = 0;
            foreach (var tmp in getReplies())
            {
                if (tmp.ID > id)
                    id = tmp.ID;
            }
            arg.ID=id+1;
            retReplay.AddLast(arg);
            return arg.ID;
        }
        public int addWork_institution(Work_institution arg)
        {
            arg.ID = getWorkInstitution().Count() + 1;
            retWorkInstitution.AddLast(arg);
            return arg.ID;
        }
        public int addUser_Project(User_Project arg)
        {
            arg.ID = getUser_Project().Count() + 1;
            retUserProject.AddLast(arg);
            return arg.ID;
        }
        public int addUser(User arg)
        {
            arg.id = getUsers().Count() + 1;
            retUsers.AddLast(arg);
            return arg.id;
        }
        public void updateUser(int ID,User arg)
        {
            User za_brishenje= arg;
            foreach (User pom in retUsers)
            {
                if (pom.id == arg.id)
                {
                    za_brishenje = pom;
                }
            }
            retUsers.Remove(za_brishenje);
            retUsers.AddLast(arg);
        }
        public int addSkill_Type(Skill_Type arg)
        {
            arg.ID = getSkill_Type().Count() + 1;
            retSkillType.AddLast(arg);
            return arg.ID;
        }
        public void addSkill(Skill arg)
        { 
            retSkill.AddLast(arg);  
        }
        public int addProject(Project arg)
        {
            arg.ID = getProject().Count() + 1;
            retProject.AddLast(arg);
            return arg.ID;
        }
        public void addPersonal_Characteristic(Personal_Characteristic arg)
        {
            retPersonalCharacteristic.AddLast(arg);
        }
        public int addNationality_option(Nationality_option arg)
        {
            arg.ID = getNationalityOption().Count() + 1;
            retNationalityOption.AddLast(arg);
            return arg.ID;
        }
        


        public void addMessages()
        {
            Message tmp = new Message();

            tmp.userID = 1;
            tmp.userID2 = 2;
            tmp.when = new DateTime(2011, 01, 07, 18, 00, 00);
            tmp.content = "Abe bote, aj staj se na LoL da mavneme edna";
            retMessages.AddFirst(tmp);

            tmp = new Message();
            tmp.userID = 2;
            tmp.userID2 = 1;
            tmp.when = new DateTime(2011, 01, 07, 18, 00, 15);
            tmp.content = "Sea bre samo da pratam domasnava";
            retMessages.AddFirst(tmp);

            tmp = new Message();
            tmp.userID = 2;
            tmp.userID2 = 1;
            tmp.when = new DateTime(2011, 01, 07, 18, 00, 30);
            tmp.content = "A ti ja prati ili ti e gajle?";
            retMessages.AddFirst(tmp);

            tmp = new Message();
            tmp.userID = 1;
            tmp.userID2 = 2;
            tmp.when = new DateTime(2011, 01, 07, 18, 01, 00);
            tmp.content = "Ehe uste vcera, mnogu bese lesna, za pola saat na napraviv";
            retMessages.AddFirst(tmp);

            tmp = new Message();
            tmp.userID = 1;
            tmp.userID2 = 2;
            tmp.when = new DateTime(2011, 01, 07, 18, 02, 00);
            tmp.content = "MORE! citas li be ej!!!";
            retMessages.AddFirst(tmp);

            tmp = new Message();
            tmp.userID = 3;
            tmp.userID2 = 1;
            tmp.when = new DateTime(2011, 01, 08, 18, 02, 00);
            tmp.content = "Fiiiideeeer, aj te cekame na LoL uste ti ni falis za 5ka";
            retMessages.AddFirst(tmp);

            tmp = new Message();
            tmp.userID = 1;
            tmp.userID2 = 3;
            tmp.when = new DateTime(2011, 01, 08, 18, 03, 00);
            tmp.content = "Eve, eve palam, Dzivri samo da oblecam";
            retMessages.AddFirst(tmp);
        }
        public void addReplies()
        {
            Reply tmp = new Reply();
            tmp.ID = 1;
            tmp.RequestNotiﬁcationID = 1;
            tmp.ReplyID = null;
            tmp.UserID = 2;
            tmp.content = "LELE BOT";
            tmp.when = new DateTime(2011, 12, 11, 18, 03, 00);
            retReplay.AddFirst(tmp);

            tmp = new Reply();
            tmp.ID = 8;
            tmp.RequestNotiﬁcationID = 5;
            tmp.ReplyID = null;
            tmp.UserID = 2;
            tmp.content = "ova so trebase da se pokaze";
            tmp.when = new DateTime(2011, 12, 11, 18, 03, 00);
            retReplay.AddFirst(tmp);
/*
            tmp = new Reply();
            tmp.ID = 9;
            tmp.RequestNotiﬁcationID = null;
            tmp.ReplyID = 8;
            tmp.UserID = 1;
            tmp.content = "e kako da ne";
            tmp.when = new DateTime(2011, 12, 11, 18, 03, 00);
            retReplay.AddFirst(tmp);*/

            tmp = new Reply();
            tmp.ID = 2;
            tmp.RequestNotiﬁcationID = null;
            tmp.ReplyID = 1;
            tmp.UserID = 2;
            tmp.content = "Trololololol lololol lololol aaaaaaaa";
            tmp.when = new DateTime(2011, 12, 11, 18, 05, 00);
            retReplay.AddFirst(tmp);
            
            tmp = new Reply();
            tmp.ID = 7;
            tmp.RequestNotiﬁcationID = null;
            tmp.ReplyID = 2;
            tmp.UserID = 1;
            tmp.content = "samo da se trolalo";
            tmp.when = new DateTime(2011, 12, 11, 19, 30, 00);
            retReplay.AddFirst(tmp);
            
            tmp = new Reply();
            tmp.ID = 3;
            tmp.RequestNotiﬁcationID = null;
            tmp.ReplyID = 2;
            tmp.UserID = 4;
            tmp.content = "C c c, i poslem ovie akademski gragjani ti bile, sramota";
            tmp.when = new DateTime(2011, 12, 11, 19, 15, 00);
            retReplay.AddFirst(tmp);

            tmp = new Reply();
            tmp.ID = 4;
            tmp.RequestNotiﬁcationID = null;
            tmp.ReplyID = 3;
            tmp.UserID = 2;
            tmp.content = "Koleshke aj ve molam ne zamarajte, ako sakate serioznost idete na drug channel ne na Troll -_- ";
            tmp.when = new DateTime(2011, 12, 11, 19, 19, 00);
            retReplay.AddFirst(tmp);

            tmp = new Reply();
            tmp.ID = 5;
            tmp.RequestNotiﬁcationID = 3;
            tmp.ReplyID = null;
            tmp.UserID = 4;
            tmp.content = "A dve primate?";
            tmp.when = new DateTime(2011, 12, 10, 19, 03, 00);
            retReplay.AddFirst(tmp);

            tmp = new Reply();
            tmp.ID = 6;
            tmp.RequestNotiﬁcationID = null;
            tmp.ReplyID = 5;
            tmp.UserID = 2;
            tmp.content = "Se razbira deka primame :] ";
            tmp.when = new DateTime(2011, 12, 10, 21, 03, 00);
            retReplay.AddFirst(tmp);
        }
        public void addGroupTypes()
        {
            Group_Type tmp = new Group_Type();
            tmp.ID = 1;
            tmp.name = "Разно";
            tmp.url = "~/Content/Pics/Troll.png";
            retGroupTypes.AddFirst(tmp);

            tmp = new Group_Type();
            tmp.ID = 2;
            tmp.name = "Веб програмирање";
            tmp.url = "~/Content/Pics/Web_Programing.png";
            retGroupTypes.AddFirst(tmp);

            tmp = new Group_Type();
            tmp.ID = 3;
            tmp.name = "Развој";
            tmp.url = "~/Content/Pics/Development.png";
            retGroupTypes.AddFirst(tmp);

            tmp = new Group_Type();
            tmp.ID = 4;
            tmp.name = "Дизајн и мултимедија";
            tmp.url = "~/Content/Pics/Design_n_Multimedia.png";
            retGroupTypes.AddFirst(tmp);

            tmp = new Group_Type();
            tmp.ID = 5;
            tmp.name = "Продажба и маркетинг";
            tmp.url = "~/Content/Pics/Sales_n_Marketing.png";
            retGroupTypes.AddFirst(tmp);

            tmp = new Group_Type();
            tmp.ID = 6;
            tmp.name = "Мрежи";
            tmp.url = "~/Content/Pics/Network_Support.png";
            retGroupTypes.AddFirst(tmp);

            tmp = new Group_Type();
            tmp.ID = 7;
            tmp.name = "Менаџмент и финансии";
            tmp.url = "~/Content/Pics/Finance_n_Management.png";
            retGroupTypes.AddFirst(tmp);

            tmp = new Group_Type();
            tmp.ID = 8;
            tmp.name = "Инженерство и производство";
            tmp.url = "~/Content/Pics/Engi_n_Manu.png";
            retGroupTypes.AddFirst(tmp);

            tmp = new Group_Type();
            tmp.ID = 9;
            tmp.name = "Академски истражувања";
            tmp.url = "~/Content/pics/Acad_n_Research.png";
            retGroupTypes.AddFirst(tmp);

        }
        public void addUsers()
        {
            User tmp = new User();

            tmp.id = 1;
            tmp.name = "Igor";
            tmp.surname = "Dimcevski";
            tmp.gender = 'm';
            tmp.year_attending = 2008;
            tmp.year_graduation = 2012;
            tmp.picture_url = null;
            retUsers.AddFirst(tmp);

            tmp = new User();
            tmp.id = 2;
            tmp.name = "Stefan";
            tmp.surname = "Spasovski";
            tmp.gender = 'm';
            tmp.year_attending = 2008;
            tmp.year_graduation = 2012;
            tmp.picture_url = "https://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-snc1/9334_1224506169080_1121781021_1783267_1922160_n.jpg";
            retUsers.AddFirst(tmp);

            tmp = new User();
            tmp.id = 3;
            tmp.name = "Milovan";
            tmp.surname = "Stolikj";
            tmp.gender = 'm';
            tmp.year_attending = 2003;
            tmp.year_graduation = 2010;
            tmp.picture_url = null;
            retUsers.AddFirst(tmp);

            tmp = new User();
            tmp.id = 4;
            tmp.name = "Marta";
            tmp.surname = "Pavleva";
            tmp.gender = 'f';
            tmp.year_attending = 2005;
            tmp.year_graduation = 2009;
            tmp.picture_url = null;
            retUsers.AddFirst(tmp);

            tmp = new User();
            tmp.id = 5;
            tmp.name = "Tome";
            tmp.surname = "Vasilev";
            tmp.gender = 'm';
            tmp.year_attending = 2010;
            tmp.year_graduation = null;
            tmp.picture_url = null;
            retUsers.AddFirst(tmp);
        }
        public void addNotifications()
        {
            Notification tmp = new Notification();

            tmp.id = 1;
            tmp.groupTypeId = 1;
            tmp.userId = 1;
            tmp.content = "Pato masovno na celo selo.";
            tmp.when = new DateTime(2011, 12, 11, 18, 00, 00);
            retNotifications.AddFirst(tmp);

            tmp = new Notification();
            tmp.id = 2;
            tmp.groupTypeId = 1;
            tmp.userId = 3;
            tmp.content = "Majka mu na Vasija";
            tmp.when = new DateTime(2011, 12, 10, 18, 00, 00);
            retNotifications.AddFirst(tmp);

            tmp = new Notification();
            tmp.id = 3;
            tmp.groupTypeId = 1;
            tmp.userId = 2;
            tmp.content = "Ima nekoj sto seuste nema najdeno stan, so cimerov ni e dosadno barame uste eden... ili edna :D";
            tmp.when = new DateTime(2011, 12, 10, 15, 03, 00);
            retNotifications.AddFirst(tmp);

            tmp = new Notification();
            tmp.id = 4;
            tmp.groupTypeId = 2;
            tmp.userId = 5;
            tmp.content = "Ima li kolegi koi se bi se nafatile da rabotat za ___________, vo glavno web strani kje treba da se pravat";
            tmp.when = new DateTime(2011, 10, 10, 10, 00, 00);
            retNotifications.AddFirst(tmp);

            tmp = new Notification();
            tmp.id = 5;
            tmp.groupTypeId = 6;
            tmp.userId = 1;
            tmp.content = "Imam problemi so mrezata, potocno mi se restartira modemot sekoj pat koga kje spustam slusalka na domasniot, kako da go popravam ova?";
            tmp.when = new DateTime(2011, 11, 10, 14, 00, 00);
            retNotifications.AddFirst(tmp);

            tmp = new Notification();
            tmp.id = 6;
            tmp.groupTypeId = 4;
            tmp.userId = 4;
            tmp.content = "Baram rabota povrzana so Flash i Photoshop, ima li nekoja ponuda?";
            tmp.when = new DateTime(2011, 09, 10, 08, 00, 00);
            retNotifications.AddFirst(tmp);

            tmp = new Notification();
            tmp.id = 7;
            tmp.groupTypeId = 2;
            tmp.userId = 2;
            tmp.content = "Samo da ve izvestam deka natprevarot za toa i toa, e tamu i tamu od tolku i tolku saat, se gladame so tie sto kje dojdat GL&HF";
            tmp.when = new DateTime(2011, 08, 10, 16, 00, 00);
            retNotifications.AddFirst(tmp);
        }
        public void addLastOpenedMessages()
        {
            LastOpenedMessage tmp = new LastOpenedMessage();

            tmp.userId1 = 2;
            tmp.userId2 = 1;
            tmp.when = new DateTime(2011, 01, 07, 18, 00, 50);

            retLastOpenedMessages.AddLast(tmp);
        }
        public void addLastVisited()
        {
            LastVisited tmp = new LastVisited();

            tmp = new LastVisited();
            tmp.userID = 1;
            tmp.groupTypeID = 1;
            tmp.when = DateTime.Now;
            retLastVisited.AddFirst(tmp);

            tmp = new LastVisited();
            tmp.userID = 1;
            tmp.groupTypeID = 6;
            tmp.when = DateTime.Now;
            retLastVisited.AddFirst(tmp);

            tmp = new LastVisited();
            tmp.userID = 4;
            tmp.groupTypeID = 4;
            tmp.when = DateTime.Now;
            retLastVisited.AddFirst(tmp);

            tmp = new LastVisited();
            tmp.userID = 5;
            tmp.groupTypeID = 2;
            tmp.when = DateTime.Now;
            retLastVisited.AddFirst(tmp);

            tmp = new LastVisited();
            tmp.userID = 3;
            tmp.groupTypeID = 1;
            tmp.when = DateTime.Now;
            retLastVisited.AddFirst(tmp);

            tmp = new LastVisited();
            tmp.userID = 2;
            tmp.groupTypeID = 1;
            tmp.when = new DateTime(2010, 01, 07, 18, 00, 00); ;
            retLastVisited.AddFirst(tmp);
            tmp = new LastVisited();

            tmp.userID = 2;
            tmp.groupTypeID = 2;
            tmp.when = new DateTime(2010, 01, 07, 18, 00, 00); ;
            retLastVisited.AddFirst(tmp);

            tmp = new LastVisited();
            tmp.userID = 4;
            tmp.groupTypeID = 1;
            tmp.when = DateTime.Now;
            retLastVisited.AddFirst(tmp);
        }
        public void addRequests()
        {
            Request tmp = new Request();

            tmp.NotificationID = 1;
            tmp.avaliable_from = DateTime.Now;
            tmp.avaliable_to = new DateTime(2012, 05, 01, 8, 30, 00);
            retRequests.AddFirst(tmp);

            tmp = new Request();
            tmp.NotificationID = 3;
            tmp.avaliable_from = DateTime.Now;
            tmp.avaliable_to = new DateTime(2012, 02, 02, 23, 55, 00);
            retRequests.AddFirst(tmp);

            tmp = new Request();
            tmp.NotificationID = 4;
            tmp.avaliable_from = DateTime.Now;
            tmp.avaliable_to = new DateTime(2011, 05, 12, 23, 55, 00);
            retRequests.AddFirst(tmp);

            tmp = new Request();
            tmp.NotificationID = 5;
            tmp.avaliable_from = DateTime.Now;
            tmp.avaliable_to = new DateTime(2012, 12, 12, 12, 12, 12);
            retRequests.AddFirst(tmp);

            tmp = new Request();
            tmp.NotificationID = 6;
            tmp.avaliable_from = DateTime.Now;
            tmp.avaliable_to = new DateTime(2012, 01, 07, 18, 00, 00);
            retRequests.AddFirst(tmp);
        }
        public void addCategoriesInCV()
        {
            Categories_in_CV tmp = new Categories_in_CV();

            tmp.Group_TypeID = 1;
            tmp.History_CVID = 1;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 2;
            tmp.History_CVID = 1;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 3;
            tmp.History_CVID = 1;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 4;
            tmp.History_CVID = 1;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 5;
            tmp.History_CVID = 1;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 6;
            tmp.History_CVID = 1;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 7;
            tmp.History_CVID = 1;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 8;
            tmp.History_CVID = 1;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 9;
            tmp.History_CVID = 1;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 1;
            tmp.History_CVID = 2;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 2;
            tmp.History_CVID = 2;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 3;
            tmp.History_CVID = 2;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 4;
            tmp.History_CVID = 2;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 5;
            tmp.History_CVID = 2;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 6;
            tmp.History_CVID = 2;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 7;
            tmp.History_CVID = 2;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 8;
            tmp.History_CVID = 2;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 9;
            tmp.History_CVID = 2;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 1;
            tmp.History_CVID = 3;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 2;
            tmp.History_CVID = 3;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 3;
            tmp.History_CVID = 3;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 4;
            tmp.History_CVID = 3;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 5;
            tmp.History_CVID = 3;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 6;
            tmp.History_CVID = 3;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 7;
            tmp.History_CVID = 3;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 8;
            tmp.History_CVID = 3;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 9;
            tmp.History_CVID = 3;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 1;
            tmp.History_CVID = 4;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 2;
            tmp.History_CVID = 4;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 3;
            tmp.History_CVID = 4;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 4;
            tmp.History_CVID = 4;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 5;
            tmp.History_CVID = 4;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 6;
            tmp.History_CVID = 4;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 7;
            tmp.History_CVID = 4;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 8;
            tmp.History_CVID = 4;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 9;
            tmp.History_CVID = 4;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 1;
            tmp.History_CVID = 5;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 2;
            tmp.History_CVID = 5;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 3;
            tmp.History_CVID = 5;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 4;
            tmp.History_CVID = 5;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 5;
            tmp.History_CVID = 5;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 6;
            tmp.History_CVID = 5;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 7;
            tmp.History_CVID = 5;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 8;
            tmp.History_CVID = 5;
            tmp.subscribed = false;
            retCategoriesInCV.AddFirst(tmp);

            tmp = new Categories_in_CV();
            tmp.Group_TypeID = 9;
            tmp.History_CVID = 5;
            tmp.subscribed = true;
            retCategoriesInCV.AddFirst(tmp);
        }
        public void addHistoryCV()
        {
            History_CV tmp = new History_CV();

            tmp.ID = 1;
            tmp.UserID = 1;
            tmp.date_of_birth = new DateTime(1989, 01, 07, 18, 00, 00);
            tmp.place_of_birth = "Skopje";
            tmp.Martial_status_optionID = 2;
            tmp.address = "ul. Bulevar br.5/3";
            tmp.phone = "075 346 432";
            tmp.Nationality_optionaID = 1;
            tmp.email = "drvosecac@bossmail.com";
            retHistoryCV.AddFirst(tmp);

            tmp = new History_CV();
            tmp.ID = 2;
            tmp.UserID = 2;
            tmp.date_of_birth = new DateTime(1989, 03, 21, 08, 00, 00);
            tmp.place_of_birth = "Skopje";
            tmp.Martial_status_optionID = 2;
            tmp.address = "ul. Kaldokolena br.7";
            tmp.phone = "077 743 293";
            tmp.Nationality_optionaID = 1;
            tmp.email = "shpicajzla@bossmail.com";
            retHistoryCV.AddFirst(tmp);

            tmp = new History_CV();
            tmp.ID = 3;
            tmp.UserID = 3;
            tmp.date_of_birth = new DateTime(1983, 11, 17, 13, 14, 00);
            tmp.place_of_birth = "Bitola";
            tmp.Martial_status_optionID = 1;
            tmp.address = "ul. Shiroksokak br.2/14";
            tmp.phone = "070 321 423";
            tmp.Nationality_optionaID = 2;
            tmp.email = "milovanecar@bossmail.com";
            retHistoryCV.AddFirst(tmp);

            tmp = new History_CV();
            tmp.ID = 4;
            tmp.UserID = 4;
            tmp.date_of_birth = new DateTime(1988, 06, 14, 16, 00, 00);
            tmp.place_of_birth = "Kumanovo";
            tmp.Martial_status_optionID = 3;
            tmp.address = "ul. Kudetumbe br.3";
            tmp.phone = "072 539 277";
            tmp.Nationality_optionaID = 1;
            tmp.email = "sexyonekissche@bossmail.com";
            retHistoryCV.AddFirst(tmp);

            tmp = new History_CV();
            tmp.ID = 5;
            tmp.UserID = 5;
            tmp.date_of_birth = new DateTime(1992, 06, 28, 01, 00, 00);
            tmp.place_of_birth = "Shtip";
            tmp.Martial_status_optionID = 2;
            tmp.address = "ul. Ondeka br.23";
            tmp.phone = "076 433 995";
            tmp.Nationality_optionaID = 1;
            tmp.email = "shmirgla007@bossmail.com";
            retHistoryCV.AddFirst(tmp);

            tmp = new History_CV();
            tmp.ID = 6;
            tmp.UserID = 5;
            tmp.date_of_birth = new DateTime(1992, 06, 28, 01, 00, 00);
            tmp.place_of_birth = "Shtip";
            tmp.Martial_status_optionID = 2;
            tmp.address = "ul. Ponataki br.7";
            tmp.phone = "076 433 995";
            tmp.Nationality_optionaID = 1;
            tmp.email = "shmirgla007@bossmail.com , karburator53@bossmail.com";
            retHistoryCV.AddFirst(tmp);

            tmp = new History_CV();
            tmp.ID = 7;
            tmp.UserID = 5;
            tmp.date_of_birth = new DateTime(1992, 06, 28, 01, 00, 00);
            tmp.place_of_birth = "Shtip";
            tmp.Martial_status_optionID = 2;
            tmp.address = "ul. Ponataki br.7";
            tmp.phone = "077 576 995";
            tmp.Nationality_optionaID = 1;
            tmp.email = "shmirgla007@bossmail.com , karburator53@bossmail.com";
            retHistoryCV.AddFirst(tmp);

            tmp = new History_CV();
            tmp.ID = 8;
            tmp.UserID = 4;
            tmp.date_of_birth = new DateTime(1988, 06, 14, 16, 00, 00);
            tmp.place_of_birth = "Kumanovo";
            tmp.Martial_status_optionID = 3;
            tmp.address = "ul. Kudetumbe br.3";
            tmp.phone = "072 539 277 , 077 539 277";
            tmp.Nationality_optionaID = 1;
            tmp.email = "sexyonekissche@bossmail.com";
            retHistoryCV.AddFirst(tmp);

            tmp = new History_CV();
            tmp.ID = 9;
            tmp.UserID = 3;
            tmp.date_of_birth = new DateTime(1983, 11, 17, 13, 14, 00);
            tmp.place_of_birth = "Bitola";
            tmp.Martial_status_optionID = 1;
            tmp.address = "ul. Udzhumpalevo br.bb";
            tmp.phone = "070 321 423";
            tmp.Nationality_optionaID = 2;
            tmp.email = "milovanecar@bossmail.com , milovanepocar@bossmail.com";
            retHistoryCV.AddFirst(tmp);
        }
        public void addProject()
        {
            Project tmp = new Project();
            tmp.ID = 1;
            tmp.History_CVID = 1;
            tmp.title = "Alumni";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 2;
            tmp.History_CVID = 1;
            tmp.title = "NesoSoBazi";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 3;
            tmp.History_CVID = 2;
            tmp.title = "AbeAjde";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 4;
            tmp.History_CVID = 3;
            tmp.title = "CickoMilovan";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 5;
            tmp.History_CVID = 9;
            tmp.title = "CickoMilovan";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 6;
            tmp.History_CVID = 9;
            tmp.title = "CickoMilovanProEdition";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 7;
            tmp.History_CVID = 4;
            tmp.title = "StikliISeZaNiv";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 8;
            tmp.History_CVID = 4;
            tmp.title = "SikliINigdeBezNiv";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 9;
            tmp.History_CVID = 8;
            tmp.title = "StikliISeZaNiv";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 10;
            tmp.History_CVID = 8;
            tmp.title = "SikliINigdeBezNiv";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 11;
            tmp.History_CVID = 5;
            tmp.title = "OdDosadaNemaBeganje";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 12;
            tmp.History_CVID = 6;
            tmp.title = "OdDosadaNemaBeganje";
            retProject.AddFirst(tmp);

            tmp = new Project();
            tmp.ID = 13;
            tmp.History_CVID = 7;
            tmp.title = "OdDosadaNemaBeganje";
            retProject.AddFirst(tmp);
        }
        public void addUserProject()
        {
            User_Project tmp = new User_Project();

            tmp.ID = 1;
            tmp.UserID = 1;
            tmp.ProjectID = 1;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 2;
            tmp.UserID = 1;
            tmp.ProjectID = 2;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 3;
            tmp.UserID = 2;
            tmp.ProjectID = 2;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 4;
            tmp.UserID = 2;
            tmp.ProjectID = 4;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 5;
            tmp.UserID = 3;
            tmp.ProjectID = 5;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 6;
            tmp.UserID = 3;
            tmp.ProjectID = 6;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 7;
            tmp.UserID = 4;
            tmp.ProjectID = 7;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 8;
            tmp.UserID = 4;
            tmp.ProjectID = 8;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 9;
            tmp.UserID = 4;
            tmp.ProjectID = 9;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 10;
            tmp.UserID = 4;
            tmp.ProjectID = 10;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 11;
            tmp.UserID = 5;
            tmp.ProjectID = 11;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 12;
            tmp.UserID = 5;
            tmp.ProjectID = 12;
            retUserProject.AddFirst(tmp);

            tmp = new User_Project();
            tmp.ID = 13;
            tmp.UserID = 5;
            tmp.ProjectID = 13;
            retUserProject.AddFirst(tmp);
        }
        public void addDescription()
        {
            Description tmp = new Description();

            tmp.User_ProjectID = 1;
            tmp.description = "Alumni za FINKI, odrzuvanje kontakt megju site koi nekogas bile del od FINKI";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 2;
            tmp.description = "Kako sto kazuva imeto neso povrzano so bazi, sto tocno mora da se vidi za da se poveruva";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 3;
            tmp.description = "stefco so vika";
            retDescription.AddFirst(tmp);


            tmp = new Description();
            tmp.User_ProjectID = 3;
            tmp.description = "Igrica AbeAjde, kade celta e da pogodish 20 prasanja po red so no pritoa odgovorite se skrieni negde na ekranot, za sekoe prasanje ima time limit";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 4;
            tmp.description = "CickoMilovan e softver za ribari, koj kazuva koja jadica, trska, mamka itn da se koristi i kade za da se fati posakuvaniot tip na riba,  se razbira ova samo gi zgolemuva sansite";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 5;
            tmp.description = "CickoMilovan e softver za ribari, koj kazuva koja jadica, trska, mamka itn da se koristi i kade za da se fati posakuvaniot tip na riba,  se razbira ova samo gi zgolemuva sansite";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 6;
            tmp.description = "Podobrena verzija na CickoMilovan softverot";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 7;
            tmp.description = "Softver so dobri soveti za stikli, koi tip, kade, koga i so sto da se nosat";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 8;
            tmp.description = "Softver koj pomaga da se objasni sto se Sikli i zasto se potrebni preku interakcija so korisnikot";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 9;
            tmp.description = "Softver so dobri soveti za stikli, koi tip, kade, koga i so sto da se nosat";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 10;
            tmp.description = "Softver koj pomaga da se objasni sto se Sikli i zasto se potrebni preku interakcija so korisnikot";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 11;
            tmp.description = "OdDosadaNemaBeganje e softver koi dava preporaki sto da se pravi za produktivno da se iskoritsti slobodnoto vreme kade ne znaeme sto da praime";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 12;
            tmp.description = "OdDosadaNemaBeganje e softver koi dava preporaki sto da se pravi za produktivno da se iskoritsti slobodnoto vreme kade ne znaeme sto da praime";
            retDescription.AddFirst(tmp);

            tmp = new Description();
            tmp.User_ProjectID = 13;
            tmp.description = "OdDosadaNemaBeganje e softver koi dava preporaki sto da se pravi za produktivno da se iskoritsti slobodnoto vreme kade ne znaeme sto da praime";
            retDescription.AddFirst(tmp);
        }
        public void addInteres()
        {
            Interes tmp = new Interes();

            tmp.Interes_TypeID = 1;
            tmp.History_CVID = 1;
            tmp.description = "Gejmanje, igranje kumjuterski igrici kako LoL (League of Legends), WoW (World of Warcraft), Skyrim, PES 2012 (Pro Evolution Soccer)...";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 2;
            tmp.History_CVID = 1;
            tmp.description = "Programiranje vo C++ i Visual C";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 2;
            tmp.History_CVID = 2;
            tmp.description = "Programiranje vo Java";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 1;
            tmp.History_CVID = 2;
            tmp.description = "Gejmanje, igranje kumjuterski igrici kako LoL (League of Legends), WoW (World of Warcraft), Skyrim, Mustafa (Cadilacs & Dinosaurs)...";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 3;
            tmp.History_CVID = 3;
            tmp.description = "Pecanje ribi, i vodni i na suvo";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 3;
            tmp.History_CVID = 9;
            tmp.description = "Pecanje ribi, i vodni i na suvo";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 4;
            tmp.History_CVID = 4;
            tmp.description = "Shopping, iskacanje, zurkanje itn";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 4;
            tmp.History_CVID = 8;
            tmp.description = "Shopping, iskacanje, zurkanje itn";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 5;
            tmp.History_CVID = 5;
            tmp.description = "Fudbal, igranje, gledanje, kladenje se povrzano so nego";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 5;
            tmp.History_CVID = 6;
            tmp.description = "Fudbal, igranje, gledanje, kladenje se povrzano so nego";
            retInteres.AddFirst(tmp);

            tmp = new Interes();
            tmp.Interes_TypeID = 5;
            tmp.History_CVID = 7;
            tmp.description = "Fudbal, igranje, gledanje, kladenje se povrzano so nego";
            retInteres.AddFirst(tmp);
        }
        public void addInteresType()
        {
            Interes_Type tmp = new Interes_Type();

            tmp.ID = 1;
            tmp.title = "Gejmanje";
            retInteresType.AddFirst(tmp);

            tmp = new Interes_Type();
            tmp.ID = 2;
            tmp.title = "Programiranje";
            retInteresType.AddFirst(tmp);

            tmp = new Interes_Type();
            tmp.ID = 3;
            tmp.title = "Ribarenje";
            retInteresType.AddFirst(tmp);

            tmp = new Interes_Type();
            tmp.ID = 4;
            tmp.title = "Shopping";
            retInteresType.AddFirst(tmp);

            tmp = new Interes_Type();
            tmp.ID = 5;
            tmp.title = "Fudbal";
            retInteresType.AddFirst(tmp);
        }
        public void addSkill()
        {
            Skill tmp = new Skill();

            tmp.Skill_TypeID = 2;
            tmp.History_CVID = 1;
            tmp.description = "Java kodiranje";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 1;
            tmp.History_CVID = 1;
            tmp.description = "C kodiranje";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 2;
            tmp.History_CVID = 2;
            tmp.description = "Java kodiranje";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 1;
            tmp.History_CVID = 2;
            tmp.description = "C kodiranje";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 3;
            tmp.History_CVID = 2;
            tmp.description = "Kreiranje android aplikacii";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 4;
            tmp.History_CVID = 3;
            tmp.description = "Web programiranje";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 4;
            tmp.History_CVID = 9;
            tmp.description = "Web programiranje";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 5;
            tmp.History_CVID = 4;
            tmp.description = "Pravenje Flash animacii";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 6;
            tmp.History_CVID = 4;
            tmp.description = "Fotoshop editiranje i crtanje";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 5;
            tmp.History_CVID = 8;
            tmp.description = "Pravenje Flash animacii";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 6;
            tmp.History_CVID = 8;
            tmp.description = "Fotoshop editiranje i crtanje";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 7;
            tmp.History_CVID = 5;
            tmp.description = "Iskusno menadzjiranje proekti";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 7;
            tmp.History_CVID = 6;
            tmp.description = "Iskusno menadzjiranje proekti";
            retSkill.AddFirst(tmp);

            tmp = new Skill();
            tmp.Skill_TypeID = 7;
            tmp.History_CVID = 7;
            tmp.description = "Iskusno menadzjiranje proekti";
            retSkill.AddFirst(tmp);
        }
        public void addSkillType()
        {
            Skill_Type tmp = new Skill_Type();

            tmp.ID = 1;
            tmp.title = "C";
            retSkillType.AddFirst(tmp);

            tmp = new Skill_Type();
            tmp.ID = 2;
            tmp.title = "Java";
            retSkillType.AddFirst(tmp);

            tmp = new Skill_Type();
            tmp.ID = 3;
            tmp.title = "Android Aplikacii";
            retSkillType.AddFirst(tmp);

            tmp = new Skill_Type();
            tmp.ID = 4;
            tmp.title = "Web programiranje";
            retSkillType.AddFirst(tmp);

            tmp = new Skill_Type();
            tmp.ID = 5;
            tmp.title = "Flash";
            retSkillType.AddFirst(tmp);

            tmp = new Skill_Type();
            tmp.ID = 6;
            tmp.title = "Photoshop";
            retSkillType.AddFirst(tmp);

            tmp = new Skill_Type();
            tmp.ID = 7;
            tmp.title = "Menadzjerstvo";
            retSkillType.AddFirst(tmp);
        }
        public void addMartialStatusOption()
        {
            Martial_status_option tmp = new Martial_status_option();

            tmp.ID = 1;
            tmp.text = "Vo brak";
            retMartialStatusOption.AddFirst(tmp);

            tmp = new Martial_status_option();
            tmp.ID = 2;
            tmp.text = "Nezhenet";
            retMartialStatusOption.AddFirst(tmp);

            tmp = new Martial_status_option();
            tmp.ID = 3;
            tmp.text = "Nemazhena";
            retMartialStatusOption.AddFirst(tmp);

            tmp = new Martial_status_option();
            tmp.ID = 4;
            tmp.text = "Razveden";
            retMartialStatusOption.AddFirst(tmp);

            tmp = new Martial_status_option();
            tmp.ID = 5;
            tmp.text = "Razvedena";
            retMartialStatusOption.AddFirst(tmp);
        }
        public void addNationalityOption()
        {
            Nationality_option tmp = new Nationality_option();

            tmp.ID = 1;
            tmp.text = "Makedonija";
            retNationalityOption.AddFirst(tmp);

            tmp = new Nationality_option();
            tmp.ID = 2;
            tmp.text = "Srbija";
            retNationalityOption.AddFirst(tmp);

            tmp = new Nationality_option();
            tmp.ID = 3;
            tmp.text = "Albanija";
            retNationalityOption.AddFirst(tmp);
        }
        public void addHistoryCVEducationInstitution()
        {
            History_CV_Education_institution tmp = new History_CV_Education_institution();

            tmp.History_CVID = 2;
            tmp.Education_institutionID = 1;
            tmp.description = "Top student, jak prosek, se iznagejmav";
            tmp.from_date = new DateTime(2008, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2011, 09, 20, 16, 30, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 2;
            tmp.Education_institutionID = 2;
            tmp.description = "Top student, jak prosek, se pomalce gejmav poso bev i demonstrator vo isto vreme";
            tmp.from_date = new DateTime(2011, 09, 09, 16, 30, 00);
            tmp.to = new DateTime(2012, 09, 10, 16, 30, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 2;
            tmp.Education_institutionID = 3;
            tmp.description = "Odlicen ucenik";
            tmp.from_date = new DateTime(2004, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2008, 09, 06, 16, 30, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 1;
            tmp.Education_institutionID = 1;
            tmp.description = "Dobar student, jak prosek, se iznagejmav";
            tmp.from_date = new DateTime(2008, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2011, 09, 09, 16, 30, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 1;
            tmp.Education_institutionID = 2;
            tmp.description = "Dobar student, jak prosek, pak se iznagejmav";
            tmp.from_date = new DateTime(2011, 09, 09, 16, 30, 00);
            tmp.to = new DateTime(2012, 09, 10, 16, 30, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 1;
            tmp.Education_institutionID = 4;
            tmp.description = "Odlicen ucenik";
            tmp.from_date = new DateTime(2004, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2008, 09, 06, 16, 30, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 3;
            tmp.Education_institutionID = 1;
            tmp.description = "Najbolje beshe, se zurkavme jako, dobar student";
            tmp.from_date = new DateTime(2003, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2010, 10, 10, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 3;
            tmp.Education_institutionID = 5;
            tmp.description = "Odlicen ali ne site petki, me mrzese taa po hemija";
            tmp.from_date = new DateTime(1999, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2003, 10, 10, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 9;
            tmp.Education_institutionID = 1;
            tmp.description = "Najbolje beshe, se zurkavme jako, dobar student";
            tmp.from_date = new DateTime(2003, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2010, 10, 10, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 9;
            tmp.Education_institutionID = 5;
            tmp.description = "Odlicen ali ne site petki, me mrzese taa po hemija";
            tmp.from_date = new DateTime(1999, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2003, 10, 06, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 4;
            tmp.Education_institutionID = 1;
            tmp.description = "Odlicna studentka, super prosek";
            tmp.from_date = new DateTime(2005, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2009, 10, 06, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 4;
            tmp.Education_institutionID = 6;
            tmp.description = "Odlicna studentka, site petki";
            tmp.from_date = new DateTime(2001, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2005, 11, 06, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 8;
            tmp.Education_institutionID = 1;
            tmp.description = "Odlicna studentka, super prosek";
            tmp.from_date = new DateTime(2005, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2009, 11, 10, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 8;
            tmp.Education_institutionID = 6;
            tmp.description = "Odlicna studentka, site petki";
            tmp.from_date = new DateTime(2001, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2005, 11, 06, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 5;
            tmp.Education_institutionID = 2;
            tmp.description = "Taze student";
            tmp.from_date = new DateTime(2010, 01, 09, 8, 30, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 5;
            tmp.Education_institutionID = 7;
            tmp.description = "Mnogu dobar";
            tmp.from_date = new DateTime(2005, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2009, 11, 06, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 6;
            tmp.Education_institutionID = 2;
            tmp.description = "Taze student";
            tmp.from_date = new DateTime(2010, 01, 09, 8, 30, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 6;
            tmp.Education_institutionID = 7;
            tmp.description = "Mnogu dobar";
            tmp.from_date = new DateTime(2005, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2009, 11, 06, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 7;
            tmp.Education_institutionID = 2;
            tmp.description = "Taze student";
            tmp.from_date = new DateTime(2010, 01, 09, 8, 30, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);

            tmp = new History_CV_Education_institution();
            tmp.History_CVID = 7;
            tmp.Education_institutionID = 7;
            tmp.description = "Mnogu dobar";
            tmp.from_date = new DateTime(2005, 01, 09, 8, 30, 00);
            tmp.to = new DateTime(2009, 11, 06, 12, 00, 00);
            retHistoryCVEducationInstitution.AddFirst(tmp);
        }
        public void addEducationInstitution()
        {
            Education_institution tmp = new Education_institution();

            tmp.ID = 1;
            tmp.title = "PMF";
            tmp.Education_TypeID = 1;
            retEducationInstitution.AddFirst(tmp);

            tmp = new Education_institution();
            tmp.ID = 2;
            tmp.title = "FINKI";
            tmp.Education_TypeID = 1;
            retEducationInstitution.AddFirst(tmp);

            tmp = new Education_institution();
            tmp.ID = 3;
            tmp.title = "Orce";
            tmp.Education_TypeID = 2;
            retEducationInstitution.AddFirst(tmp);

            tmp = new Education_institution();
            tmp.ID = 4;
            tmp.title = "Boris";
            tmp.Education_TypeID = 2;
            retEducationInstitution.AddFirst(tmp);

            tmp = new Education_institution();
            tmp.ID = 5;
            tmp.title = "NKSSH";
            tmp.Education_TypeID = 2;
            retEducationInstitution.AddFirst(tmp);

            tmp = new Education_institution();
            tmp.ID = 6;
            tmp.title = "QSI";
            tmp.Education_TypeID = 2;
            retEducationInstitution.AddFirst(tmp);

            tmp = new Education_institution();
            tmp.ID = 7;
            tmp.title = "JBT";
            tmp.Education_TypeID = 2;
            retEducationInstitution.AddFirst(tmp);
        }
        public void addEducationType()
        {
            Education_Type tmp = new Education_Type();

            tmp.ID = 1;
            tmp.text = "Faks";
            retEducationType.AddFirst(tmp);

            tmp = new Education_Type();
            tmp.ID = 2;
            tmp.text = "Sredno";
            retEducationType.AddFirst(tmp);
        }
        public void addHistoryCVWorkInstitution()
        {
            History_CV_Work_institution tmp = new History_CV_Work_institution();

            tmp.History_CVID = 2;
            tmp.Work_institutionID = 2;
            tmp.from_date = new DateTime(2011, 01, 07, 8, 30, 00);
            tmp.to = new DateTime(2011, 01, 09, 16, 30, 00);
            retHistoryCVWorkInstitution.AddFirst(tmp);

            tmp = new History_CV_Work_institution();
            tmp.History_CVID = 1;
            tmp.Work_institutionID = 1;
            tmp.from_date = new DateTime(2011, 01, 06, 8, 30, 00);
            tmp.to = new DateTime(2011, 01, 09, 16, 30, 00);
            retHistoryCVWorkInstitution.AddFirst(tmp);

            tmp = new History_CV_Work_institution();
            tmp.History_CVID = 3;
            tmp.Work_institutionID = 1;
            tmp.from_date = new DateTime(2004, 01, 06, 8, 30, 00);
            retHistoryCVWorkInstitution.AddFirst(tmp);

            tmp = new History_CV_Work_institution();
            tmp.History_CVID = 9;
            tmp.Work_institutionID = 1;
            tmp.from_date = new DateTime(2004, 01, 06, 8, 30, 00);
            retHistoryCVWorkInstitution.AddFirst(tmp);

            tmp = new History_CV_Work_institution();
            tmp.History_CVID = 4;
            tmp.Work_institutionID = 3;
            tmp.from_date = new DateTime(2011, 01, 06, 8, 30, 00);
            tmp.to = new DateTime(2011, 01, 09, 16, 30, 00);
            retHistoryCVWorkInstitution.AddFirst(tmp);

            tmp = new History_CV_Work_institution();
            tmp.History_CVID = 8;
            tmp.Work_institutionID = 3;
            tmp.from_date = new DateTime(2010, 01, 06, 8, 30, 00);
            tmp.to = new DateTime(2010, 01, 09, 16, 30, 00);
            retHistoryCVWorkInstitution.AddFirst(tmp);
        }
        public void addWorkInstitution()
        {
            Work_institution tmp = new Work_institution();

            tmp.ID = 1;
            tmp.title = "Doma";
            retWorkInstitution.AddFirst(tmp);

            tmp = new Work_institution();
            tmp.ID = 2;
            tmp.title = "Netcetera";
            retWorkInstitution.AddFirst(tmp);

            tmp = new Work_institution();
            tmp.ID = 3;
            tmp.title = "Two";
            retWorkInstitution.AddFirst(tmp);
        }
        public void addPersonalCharacteristic()
        {
            Personal_Characteristic tmp = new Personal_Characteristic();

            tmp.History_CVID = 2;
            tmp.order = 1;
            tmp.title = "Team player";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 2;
            tmp.order = 2;
            tmp.title = "Gets stuff done";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 2;
            tmp.order = 3;
            tmp.title = "Rekurzii mu idat";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 1;
            tmp.order = 4;
            tmp.title = "Preferira rusko programiranje";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 1;
            tmp.order = 5;
            tmp.title = "profi debager";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 3;
            tmp.order = 6;
            tmp.title = "Shef";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 9;
            tmp.order = 7;
            tmp.title = "Shef";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 4;
            tmp.order = 8;
            tmp.title = "Solo igrac";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 8;
            tmp.order = 9;
            tmp.title = "Solo igrac";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 5;
            tmp.order = 10;
            tmp.title = "Inovativen";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 6;
            tmp.order = 11;
            tmp.title = "Inovativen";
            retPersonalCharacteristic.AddFirst(tmp);

            tmp = new Personal_Characteristic();
            tmp.History_CVID = 7;
            tmp.order = 12;
            tmp.title = "Inovativen";
            retPersonalCharacteristic.AddFirst(tmp);
        }
        public void addCourse()
        {
            Course tmp = new Course();

            tmp.ID = 1;
            tmp.History_CVID = 1;
            tmp.title = "Bazi 2";
            tmp.description = "Ko bazi 1 ali baegi poteshko";
            tmp.from_date = new DateTime(2011, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2011, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 2;
            tmp.History_CVID = 1;
            tmp.title = "Istorija na umetnosta";
            tmp.description = "samo ovoj ne zvucese tapa od izbornite";
            tmp.from_date = new DateTime(2011, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2011, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 3;
            tmp.History_CVID = 1;
            tmp.title = "DAS";
            tmp.description = "posto e sreden rod na germanski";
            tmp.from_date = new DateTime(2011, 12, 01, 09, 00, 50);
            tmp.to = new DateTime(2011, 12, 05, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 4;
            tmp.History_CVID = 2;
            tmp.title = "Bazi 2";
            tmp.description = "Ko bazi 1 ali baegi poteshko";
            tmp.from_date = new DateTime(2011, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2011, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 5;
            tmp.History_CVID = 2;
            tmp.title = "KOS";
            tmp.description = "poso me potsekja na kosinus";
            tmp.from_date = new DateTime(2011, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2011, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 6;
            tmp.History_CVID = 2;
            tmp.title = "ICK";
            tmp.description = "bless ya";
            tmp.from_date = new DateTime(2011, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2011, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 7;
            tmp.History_CVID = 3;
            tmp.title = "History of Pecanje";
            tmp.description = "moze casovi da im drzam";
            tmp.from_date = new DateTime(2008, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2008, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 8;
            tmp.History_CVID = 9;
            tmp.title = "History of Pecanje";
            tmp.description = "moze casovi da im drzam";
            tmp.from_date = new DateTime(2008, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2008, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 9;
            tmp.History_CVID = 4;
            tmp.title = "OnP";
            tmp.description = "boza cas";
            tmp.from_date = new DateTime(2008, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2008, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 10;
            tmp.History_CVID = 8;
            tmp.title = "OnP";
            tmp.description = "boza cas";
            tmp.from_date = new DateTime(2008, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2008, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 11;
            tmp.History_CVID = 5;
            tmp.title = "Angliski";
            tmp.description = "ko spanska serija da gleam, beter duri";
            tmp.from_date = new DateTime(2010, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2010, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 12;
            tmp.History_CVID = 6;
            tmp.title = "Angliski";
            tmp.description = "ko spanska serija da gleam, beter duri";
            tmp.from_date = new DateTime(2010, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2010, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);

            tmp = new Course();
            tmp.ID = 13;
            tmp.History_CVID = 7;
            tmp.title = "Angliski";
            tmp.description = "ko spanska serija da gleam, beter duri";
            tmp.from_date = new DateTime(2010, 01, 09, 09, 00, 50);
            tmp.to = new DateTime(2010, 12, 12, 18, 00, 50);
            retCourse.AddFirst(tmp);
        }
        public void addAchievement()
        {
            Achievement tmp = new Achievement();

            tmp.ID = 1;
            tmp.History_CVID = 2;
            tmp.what = "Beta key sa kotor mmorgp-to";
            tmp.description = "samo ogranicen broj lugje go imaat ova, i jas sum 1 od niv";
            tmp.when = new DateTime(2011, 01, 09, 09, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 2;
            tmp.History_CVID = 2;
            tmp.what = "30ka na LoL";
            tmp.description = "poso vredi :D";
            tmp.when = new DateTime(2011, 01, 09, 09, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 3;
            tmp.History_CVID = 2;
            tmp.what = "2ro mesto republicki matematika";
            tmp.description = "";
            tmp.when = new DateTime(2007, 05, 05, 09, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 4;
            tmp.History_CVID = 1;
            tmp.what = "80ka wow";
            tmp.description = "poso ne sekoj moze bez sifri";
            tmp.when = new DateTime(2011, 05, 05, 09, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 5;
            tmp.History_CVID = 1;
            tmp.what = "perfect score lol";
            tmp.description = "0 umreno ali dosta ubistva i asistencii";
            tmp.when = new DateTime(2011, 02, 12, 09, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 6;
            tmp.History_CVID = 3;
            tmp.what = "upecano 3 ribi u edna vecer";
            tmp.description = "i vodni i drugi :D";
            tmp.when = new DateTime(2008, 02, 02, 09, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 7;
            tmp.History_CVID = 9;
            tmp.what = "upecano 3 ribi u edna vecer";
            tmp.description = "i vodni i drugi :D";
            tmp.when = new DateTime(2008, 02, 02, 09, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 8;
            tmp.History_CVID = 4;
            tmp.what = "1 mesto u kriket";
            tmp.description = "ali sama u konkurencija bev";
            tmp.when = new DateTime(2007, 04, 08, 07, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 9;
            tmp.History_CVID = 8;
            tmp.what = "1 mesto u kriket";
            tmp.description = "ali sama u konkurencija bev";
            tmp.when = new DateTime(2007, 04, 08, 07, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 10;
            tmp.History_CVID = 5;
            tmp.what = "sportska";
            tmp.description = "100 000 gi oshuriv";
            tmp.when = new DateTime(2007, 04, 08, 07, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 11;
            tmp.History_CVID = 6;
            tmp.what = "sportska";
            tmp.description = "100 000 gi oshuriv";
            tmp.when = new DateTime(2007, 04, 08, 07, 00, 50);
            retAchievement.AddFirst(tmp);

            tmp = new Achievement();
            tmp.ID = 12;
            tmp.History_CVID = 7;
            tmp.what = "sportska";
            tmp.description = "100 000 gi oshuriv";
            tmp.when = new DateTime(2007, 04, 08, 07, 00, 50);
            retAchievement.AddFirst(tmp);
        }

        public int addAchievement(Achievement arg)
        {
            arg.ID = getAchievement().Count() + 1;
            retAchievement.AddLast(arg);
            return arg.ID;
        }

        public void addCategories_in_CV(Categories_in_CV arg)
        {
            retCategoriesInCV.AddLast(arg);
        }

        public int addCourse(Course arg)
        { 
            arg.ID = getCourse().Count() + 1;
            retCourse.AddLast(arg);
            return arg.ID;
        }

        public void addDescription(Description arg)
        {
            retDescription.AddLast(arg);
        }

        public int addEducation_institution(Education_institution arg)
        {
            arg.ID = getEducationInstitution().Count() + 1;
            retEducationInstitution.AddLast(arg);
            return arg.ID;
        }

        public int addEducation_Type(Education_Type arg)
        {
            arg.ID = getEducationType().Count() + 1;
            retEducationType.AddLast(arg);
            return arg.ID;
        }

        public int addGroup_Type(Group_Type arg)
        {
            arg.ID = getGroup_Types().Count() + 1;
            retGroupTypes.AddLast(arg);
            return arg.ID;
        }

        public int addHistory_CV(History_CV arg)
        {
            arg.ID = getHistory_CV().Count() + 1;
            retHistoryCV.AddLast(arg);
            return arg.ID;
        }

        public void addHistory_CV_Education_institution(History_CV_Education_institution arg)
        {
            retHistoryCVEducationInstitution.AddLast(arg);
        }

        public void addHistory_CV_Work_institution(History_CV_Work_institution arg)
        {
            retHistoryCVWorkInstitution.AddLast(arg);
        }

        public void addInteres(Interes arg)
        {
            retInteres.AddLast(arg);
        }

        public int addInteres_Type(Interes_Type arg)
        {
            arg.ID = getInteres_Type().Count() + 1;
            retInteresType.AddLast(arg);
            return arg.ID;
        }

        public void addLastOpenedMessage(LastOpenedMessage arg)
        {
            retLastOpenedMessages.AddLast(arg);
        }

        public void addLastVisited(LastVisited arg)
        {
            retLastVisited.AddLast(arg);
        }

        public int addMartial_status_option(Martial_status_option arg)
        {
            arg.ID = getMartialStatusOption().Count() + 1;
            retMartialStatusOption.AddLast(arg);
            return arg.ID;
        }

        public FakeRepository()
        {
            retMessages = new LinkedList<Message>();
            addMessages();
            retReplay = new LinkedList<Reply>();
            addReplies();
            retGroupTypes = new LinkedList<Group_Type>();
            addGroupTypes();
            retUsers = new LinkedList<User>();
            addUsers();
            retNotifications = new LinkedList<Notification>();
            addNotifications();
            retLastOpenedMessages = new LinkedList<LastOpenedMessage>();
            addLastOpenedMessages();
            retLastVisited = new LinkedList<LastVisited>();
            addLastVisited();
            retRequests = new LinkedList<Request>();
            addRequests();
            retCategoriesInCV = new LinkedList<Categories_in_CV>();
            addCategoriesInCV();
            retHistoryCV = new LinkedList<History_CV>();
            addHistoryCV();
            retProject = new LinkedList<Project>();
            addProject();
            retUserProject = new LinkedList<User_Project>();
            addUserProject();
            retDescription = new LinkedList<Description>();
            addDescription();
            retInteres = new LinkedList<Interes>();
            addInteres();
            retInteresType = new LinkedList<Interes_Type>();
            addInteresType();
            retSkill = new LinkedList<Skill>();
            addSkill();
            retSkillType = new LinkedList<Skill_Type>();
            addSkillType();
            retMartialStatusOption = new LinkedList<Martial_status_option>();
            addMartialStatusOption();
            retNationalityOption = new LinkedList<Nationality_option>();
            addNationalityOption();
            retHistoryCVEducationInstitution = new LinkedList<History_CV_Education_institution>();
            addHistoryCVEducationInstitution();
            retEducationInstitution = new LinkedList<Education_institution>();
            addEducationInstitution();
            retEducationType = new LinkedList<Education_Type>();
            addEducationType();
            retHistoryCVWorkInstitution = new LinkedList<History_CV_Work_institution>();
            addHistoryCVWorkInstitution();
            retWorkInstitution = new LinkedList<Work_institution>();
            addWorkInstitution();
            retPersonalCharacteristic = new LinkedList<Personal_Characteristic>();
            addPersonalCharacteristic();
            retCourse = new LinkedList<Course>();
            addCourse();
            retAchievement = new LinkedList<Achievement>();
            addAchievement();
        }

        public IQueryable<Reply> getReplies()
        {return retReplay.AsQueryable();}

        public IQueryable<Group_Type> getGroup_Types()
        {return retGroupTypes.AsQueryable();}

        public IQueryable<User> getUsers()
        {return retUsers.AsQueryable();}

        public IQueryable<Notification> getNotifications()
        {return retNotifications.AsQueryable();}

        public IQueryable<Request> getRequests()
        {return retRequests.AsQueryable();}

        public IQueryable<LastVisited> getLastVisited()
        {return retLastVisited.AsQueryable();}

        public IQueryable<Message> getMessage()
        {return retMessages.AsQueryable();}

         public IQueryable<Categories_in_CV> getCategories_in_CV()
        {return retCategoriesInCV.AsQueryable();}

         public IQueryable<History_CV> getHistory_CV()
         {return retHistoryCV.AsQueryable();}

        public IQueryable<Project> getProject()
         { return retProject.AsQueryable(); }

        public IQueryable<User_Project> getUser_Project()
        { return retUserProject.AsQueryable(); }

         public IQueryable<Description> getDescription()
        { return retDescription.AsQueryable(); }

        public IQueryable<Interes> getInteres()
         {return retInteres.AsQueryable();}

        public IQueryable<Interes_Type> getInteres_Type()
        {return retInteresType.AsQueryable();}

        public IQueryable<Skill> getSkill()
        {return retSkill.AsQueryable();}

        public IQueryable<Skill_Type> getSkill_Type()
         {return retSkillType.AsQueryable();}

        public IQueryable<LastOpenedMessage> getLastOpenedMessage()
        {return retLastOpenedMessages.AsQueryable();}

        public IQueryable<Martial_status_option> getMartialStatusOption()
        { return retMartialStatusOption.AsQueryable(); }

        public IQueryable<Nationality_option> getNationalityOption()
        { return retNationalityOption.AsQueryable(); }

        public IQueryable<History_CV_Education_institution> getHistoryCVEducationInstitution()
        { return retHistoryCVEducationInstitution.AsQueryable(); }

        public IQueryable<Education_institution> getEducationInstitution()
        { return retEducationInstitution.AsQueryable(); }

        public IQueryable<Education_Type> getEducationType()
        { return retEducationType.AsQueryable(); }

        public IQueryable<History_CV_Work_institution> getHistoryCVWorkInstitution()
        { return retHistoryCVWorkInstitution.AsQueryable(); }

        public IQueryable<Work_institution> getWorkInstitution()
        { return retWorkInstitution.AsQueryable(); }

        public IQueryable<Personal_Characteristic> getPersonalCharacteristic()
        { return retPersonalCharacteristic.AsQueryable(); }

        public IQueryable<Course> getCourse()
        { return retCourse.AsQueryable(); }

        public IQueryable<Achievement> getAchievement()
        { return retAchievement.AsQueryable(); }
    }
}